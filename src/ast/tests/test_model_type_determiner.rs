use crate::ast::impls::model_type_determiner::ModelTypeDeterminer;
use crate::ast::impls::type_determiner::TypeDeterminer;
use crate::ast::proto::IModelTypeDeterminer;
use crate::ast::types::model::ModelType::RefToModel;
use crate::ast::types::model::{
    Finalizer, ModelField, ModelType, PreValidatedModelField, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, ValidatedModule};
use crate::ast::{pre_compiler, IPreCompiler};
use std::collections::BTreeMap;

struct Setup {
    global: PreCompiled,
    module: ValidatedModule,
    mtd: ModelTypeDeterminer<TypeDeterminer>,
}

impl Setup {
    fn new() -> Self {
        let mut pc = pre_compiler();
        pc.init();
        let known = ValidatedModel {
            name: "mod.known".to_string(),
            tp: ModelType::StdInt,
            finalizer: Finalizer::Struct,
            ..Default::default()
        };
        let unknown = ValidatedModel {
            name: "mod.unknown".to_string(),
            tp: ModelType::Unknown,
            finalizer: Finalizer::Outfield("a".to_string()),
            ..Default::default()
        };
        let module = ValidatedModule {
            name: "mod".to_string(),
            imports: vec!["std".into()],
            alphabets: BTreeMap::from([("a".to_string(), "abc".to_string())]),
            tokens: BTreeMap::from([("t".to_string(), "token".to_string())]),
            models: BTreeMap::from([
                (known.name.to_string(), known),
                (unknown.name.to_string(), unknown),
            ]),
            custom_code: "".to_string(),
        };
        Self {
            global: pc.take(),
            module,
            mtd: Default::default(),
        }
    }

    fn determine_type(&self, model: &ValidatedModel) -> Result<ModelType, String> {
        self.mtd
            .determine_type(&self.global, &self.module, model)
            .map_err(|e| e.to_string())
    }
}

#[test]
fn test_mdt_custom_ok() {
    let setup = Setup::new();
    let res = setup.determine_type(&ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::StdReal,
        finalizer: Finalizer::Func("foo".to_string()),
        ..Default::default()
    });
    assert_eq!(res.unwrap(), ModelType::StdReal)
}

#[test]
fn test_mdt_custom_err() {
    let setup = Setup::new();
    let res = setup.determine_type(&ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        finalizer: Finalizer::Func("foo".to_string()),
        ..Default::default()
    });
    assert_eq!(
        res.unwrap_err(),
        "model has custom finalizer, so type must be set explicit",
    )
}

#[test]
fn test_mdt_struct() {
    let setup = Setup::new();
    let res = setup.determine_type(&ValidatedModel {
        name: "mod.x".to_string(),
        parser: Default::default(),
        tp: ModelType::Unknown,
        finalizer: Finalizer::Struct,
        pre_validated_fields: vec![PreValidatedModelField {
            name: "a".to_string(),
            variants: vec![ModelType::Unknown],
        }],
        fields: vec![ModelField {
            name: "a".to_string(),
            tp: ModelType::Unknown,
        }],
    });
    assert_eq!(res.unwrap(), ModelType::Model("mod.x".to_string()),)
}

#[test]
fn test_mdt_out_field_ok() {
    let setup = Setup::new();
    let res = setup.determine_type(&ValidatedModel {
        name: "mod.x".to_string(),
        parser: Default::default(),
        tp: ModelType::Unknown,
        finalizer: Finalizer::Outfield("a".to_string()),
        pre_validated_fields: vec![],
        fields: vec![ModelField {
            name: "a".to_string(),
            tp: ModelType::RefToModel("mod.known".to_string()),
        }],
    });
    assert_eq!(res.unwrap(), ModelType::StdInt)
}

#[test]
fn test_mdt_out_field_unk() {
    let setup = Setup::new();
    let res = setup.determine_type(&ValidatedModel {
        name: "mod.x".to_string(),
        parser: Default::default(),
        tp: ModelType::Unknown,
        finalizer: Finalizer::Outfield("a".to_string()),
        pre_validated_fields: vec![],
        fields: vec![ModelField {
            name: "a".to_string(),
            tp: ModelType::RefToModel("mod.unknown".to_string()),
        }],
    });
    assert_eq!(res.unwrap(), RefToModel("mod.unknown".to_string()))
}
