use crate::ast::impls::alphabet_maker::AlphabetMaker;
use crate::ast::proto::IAlphabetMaker;

#[test]
fn test_sort_and_uniq() {
    assert_eq!(AlphabetMaker::default().make("dacbd"), "abcd",)
}
