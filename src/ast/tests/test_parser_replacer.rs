use crate::ast::impls::parser_replacer::ParserReplacer;
use crate::ast::proto::IParserReplacer;
use crate::ast::types::model::ParserBody;

fn replace_with_stop(exp: ParserBody) -> Box<dyn Fn(&ParserBody) -> Option<ParserBody>> {
    Box::new(move |body| -> Option<ParserBody> {
        if *body == exp {
            Some(ParserBody::Stop)
        } else {
            None
        }
    })
}

#[test]
fn test_replace_in_tree() {
    use ParserBody::*;

    let replacer = ParserReplacer::default();
    let tree = And(vec![
        Or(vec![
            Token("t".to_string()).into_prs(),
            Range("a".to_string()).into_prs(),
        ])
        .into_prs(),
        Opt(Model("m".to_string()).into_bprs()).into_prs(),
    ])
    .into_prs();
    let replaced = replacer.replace(&replace_with_stop(Token("t".to_string())), &tree);
    let new_tree = replaced.unwrap();
    assert_eq!(format!("{}", new_tree), "((! | Ra) + $m?)");
    let replaced = replacer.replace(&replace_with_stop(Model("m".to_string())), &tree);
    let new_tree = replaced.unwrap();
    assert_eq!(format!("{}", new_tree), "((Tt | Ra) + !?)",);
    let replaced = replacer.replace(&replace_with_stop(Model("x".to_string())), &tree);
    assert!(replaced.is_none());
}
