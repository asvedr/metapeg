use crate::ast::impls::model_fields_determiner::ModelFieldsDeterminer;
use crate::ast::impls::type_determiner::TypeDeterminer;
use crate::ast::proto::IModelFieldsDeterminer;
use crate::ast::types::model::{
    Finalizer, ModelField, ModelType, Parser, ParserBody, PreValidatedModelField, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, ValidatedModule};
use crate::ast::{pre_compiler, IPreCompiler};
use std::collections::BTreeMap;

struct Setup {
    global: PreCompiled,
    module: ValidatedModule,
    mfd: ModelFieldsDeterminer<TypeDeterminer>,
}

impl Setup {
    fn new() -> Self {
        let mut pc = pre_compiler();
        pc.init();
        let mdl_i = ValidatedModel {
            name: "mod.i".to_string(),
            tp: ModelType::StdInt,
            finalizer: Finalizer::Outfield("a".to_string()),
            ..Default::default()
        };
        let mdl_s = ValidatedModel {
            name: "mod.s".to_string(),
            tp: ModelType::Model("mod.s".to_string()),
            finalizer: Finalizer::Struct,
            ..Default::default()
        };
        let mdl_u = ValidatedModel {
            name: "mod.u".to_string(),
            tp: ModelType::Unknown,
            finalizer: Finalizer::Outfield("a".to_string()),
            ..Default::default()
        };
        let module = ValidatedModule {
            name: "mod".to_string(),
            models: BTreeMap::from([
                (mdl_i.name.clone(), mdl_i),
                (mdl_s.name.clone(), mdl_s),
                (mdl_u.name.clone(), mdl_u),
            ]),
            ..Default::default()
        };
        Self {
            global: pc.take(),
            module,
            mfd: Default::default(),
        }
    }

    fn determine_fields(
        &self,
        model: &ValidatedModel,
    ) -> Result<Vec<PreValidatedModelField>, String> {
        self.mfd
            .determine_fields(&self.global, &self.module, model)
            .map_err(|e| e.to_string())
    }

    fn ensure_fields(&self, model: &ValidatedModel) -> Result<Vec<ModelField>, String> {
        self.mfd
            .ensure_fields(&self.global, &self.module, model)
            .map_err(|e| e.to_string())
    }
}

#[test]
fn test_determine_ok() {
    let setup = Setup::new();
    let model = ValidatedModel {
        name: "mod.x".to_string(),
        parser: ParserBody::Or(vec![
            ParserBody::And(vec![
                ParserBody::Token("mod.t".to_string()).into_named_prs("t"),
                ParserBody::Model("mod.s".to_string()).into_named_prs("m"),
            ])
            .into_prs(),
            ParserBody::OneOrMore(ParserBody::Model("std.int".to_string()).into_bprs())
                .into_named_prs("l"),
        ])
        .into_prs(),
        tp: Default::default(),
        finalizer: Default::default(),
        ..Default::default()
    };
    let pre = setup.determine_fields(&model).unwrap();
    assert_eq!(
        pre,
        &[
            PreValidatedModelField {
                name: "l".to_string(),
                variants: vec![ModelType::List(Box::new(ModelType::StdInt))],
            },
            PreValidatedModelField {
                name: "m".to_string(),
                variants: vec![ModelType::Model("mod.s".to_string())],
            },
            PreValidatedModelField {
                name: "t".to_string(),
                variants: vec![ModelType::StdStr],
            },
        ]
    )
}

#[test]
fn test_determine_var_in_check() {
    let setup = Setup::new();
    let model = ValidatedModel {
        name: "mod.x".to_string(),
        parser: ParserBody::IfFollow(
            ParserBody::Token("mod.t".to_string()).into_bprs(),
            ParserBody::Model("mod.s".to_string()).into_named_bprs("m"),
        )
        .into_prs(),
        tp: Default::default(),
        finalizer: Default::default(),
        ..Default::default()
    };
    let err = setup.determine_fields(&model).unwrap_err();
    assert_eq!(err, "var can not be declared in section 'if-follow'");

    let model = ValidatedModel {
        name: "mod.x".to_string(),
        parser: ParserBody::IfNotFollow(
            ParserBody::Token("mod.t".to_string()).into_bprs(),
            ParserBody::Model("mod.s".to_string()).into_named_bprs("m"),
        )
        .into_prs(),
        tp: Default::default(),
        finalizer: Default::default(),
        ..Default::default()
    };
    let err = setup.determine_fields(&model).unwrap_err();
    assert_eq!(err, "var can not be declared in section 'if-not-follow'")
}

#[test]
fn test_determine_var_in_list() {
    let setup = Setup::new();
    let model = ValidatedModel {
        name: "mod.x".to_string(),
        parser: ParserBody::OneOrMore(ParserBody::Model("mod.s".to_string()).into_named_bprs("m"))
            .into_prs(),
        tp: Default::default(),
        finalizer: Default::default(),
        ..Default::default()
    };
    let err = setup.determine_fields(&model).unwrap_err();
    assert_eq!(err, "var can not be declared in section '+'");

    let model = ValidatedModel {
        name: "mod.x".to_string(),
        parser: ParserBody::ZeroOrMore(ParserBody::Model("mod.s".to_string()).into_named_bprs("m"))
            .into_prs(),
        tp: Default::default(),
        finalizer: Default::default(),
        ..Default::default()
    };
    let err = setup.determine_fields(&model).unwrap_err();
    assert_eq!(err, "var can not be declared in section '*'");
}

#[test]
fn test_ensure_ok() {
    let setup = Setup::new();
    let mut model = ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        ..Default::default()
    };
    model.set_pre_validated(vec![PreValidatedModelField {
        name: "f".to_string(),
        variants: vec![ModelType::RefToModel("std.int".to_string())],
    }]);
    let fields = setup.ensure_fields(&model).unwrap();
    assert_eq!(
        fields,
        &[ModelField {
            name: "f".to_string(),
            tp: ModelType::StdInt
        }],
    )
}

#[test]
fn test_ensure_unknown() {
    let setup = Setup::new();
    let mut model = ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        ..Default::default()
    };
    model.set_pre_validated(vec![PreValidatedModelField {
        name: "f".to_string(),
        variants: vec![ModelType::RefToModel("mod.u".to_string())],
    }]);
    let fields = setup.ensure_fields(&model).unwrap();
    assert_eq!(
        fields,
        &[ModelField {
            name: "f".to_string(),
            tp: ModelType::Unknown
        }],
    );

    model.set_pre_validated(vec![PreValidatedModelField {
        name: "f".to_string(),
        variants: vec![
            ModelType::RefToModel("mod.i".to_string()),
            ModelType::RefToModel("mod.u".to_string()),
        ],
    }]);
    let fields = setup.ensure_fields(&model).unwrap();
    assert_eq!(
        fields,
        &[ModelField {
            name: "f".to_string(),
            tp: ModelType::Unknown
        }],
    )
}

#[test]
fn test_ensure_fold_sufficient_types() {
    let setup = Setup::new();
    let mut model = ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        ..Default::default()
    };
    model.set_pre_validated(vec![PreValidatedModelField {
        name: "f".to_string(),
        variants: vec![
            ModelType::RefToModel("mod.i".to_string()),
            ModelType::RefToModel("std.int".to_string()),
        ],
    }]);
    let fields = setup.ensure_fields(&model).unwrap();
    assert_eq!(
        fields,
        &[ModelField {
            name: "f".to_string(),
            tp: ModelType::StdInt
        }],
    )
}

#[test]
fn test_ensure_fold_insufficient_types() {
    let setup = Setup::new();
    let mut model = ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        ..Default::default()
    };
    model.set_pre_validated(vec![PreValidatedModelField {
        name: "f".to_string(),
        variants: vec![
            ModelType::RefToModel("mod.i".to_string()),
            ModelType::RefToModel("mod.s".to_string()),
        ],
    }]);
    let err = setup.ensure_fields(&model).unwrap_err();
    assert_eq!(err, "field f: incompatible types std.int and $mod.s");
}

#[test]
fn test_field_list() {
    let setup = Setup::new();
    let mut model = ValidatedModel {
        name: "x".to_string(),
        tp: ModelType::Unknown,
        parser: Parser {
            field: Some("out".to_string()),
            body: ParserBody::OneOrMore(ParserBody::Model("std.int".to_string()).into_bprs()),
            fatal_err: false,
        },
        finalizer: Finalizer::Outfield("out".to_string()),
        ..Default::default()
    };
    let fields = setup.determine_fields(&model).unwrap();
    model.set_pre_validated(fields);
    let fields = setup.ensure_fields(&model).unwrap();
    assert_eq!(
        fields,
        &[ModelField {
            name: "out".to_string(),
            tp: ModelType::List(Box::new(ModelType::StdInt)),
        }]
    );
}
