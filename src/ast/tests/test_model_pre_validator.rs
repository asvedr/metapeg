use crate::ast::impls::model_pre_validator::ModelPreValidator;
use crate::ast::proto::IModelPreValidator;
use crate::ast::types::model::{
    Finalizer, ModelType, Parser, ParserBody, RawModel, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, RawModule};
use crate::ast::{pre_compiler, IPreCompiler};

struct Setup {
    state: PreCompiled,
    module: RawModule,
    mpv: ModelPreValidator,
}

impl Setup {
    fn new() -> Self {
        let mut pc = pre_compiler();
        pc.init();
        let module = RawModule {
            name: "mod".to_string(),
            imports: vec!["std".into()],
            alphabets: vec![("a".to_string(), "abc".to_string())],
            tokens: vec![("t".to_string(), "tkn".to_string())],
            models: vec![RawModel {
                name: "m".to_string(),
                ..Default::default()
            }],
            custom_code: "".to_string(),
        };
        Self {
            state: pc.take(),
            module,
            mpv: ModelPreValidator::default(),
        }
    }

    fn pre_validate(&self, model: &RawModel) -> Result<ValidatedModel, String> {
        self.mpv
            .pre_validate(&self.state, &self.module, model)
            .map_err(|e| e.to_string())
    }
}

#[test]
fn test_mpv_bad_alph() {
    let setup = Setup::new();
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Range("b".to_string()).into_prs(),
        ..Default::default()
    };
    let err = setup.pre_validate(&model).unwrap_err();
    assert_eq!(err, "alphabet mod.b not found")
}

#[test]
fn test_mpv_bad_token() {
    let setup = Setup::new();
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Token("b".to_string()).into_prs(),
        ..Default::default()
    };
    let err = setup.pre_validate(&model).unwrap_err();
    assert_eq!(err, "token mod.b not found")
}

#[test]
fn test_mpv_bad_model() {
    let setup = Setup::new();
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Model("b".to_string()).into_prs(),
        ..Default::default()
    };
    let err = setup.pre_validate(&model).unwrap_err();
    assert_eq!(err, "model mod.b not found");
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Model("std.b".to_string()).into_prs(),
        ..Default::default()
    };
    let err = setup.pre_validate(&model).unwrap_err();
    assert_eq!(err, "model std.b not found");
}

#[test]
fn test_mpv_not_imported() {
    let setup = Setup::new();
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Model("unimp.b".to_string()).into_prs(),
        ..Default::default()
    };
    let err = setup.pre_validate(&model).unwrap_err();
    assert_eq!(err, "module unimp is not imported");
}

#[test]
fn test_mpv_ok() {
    let setup = Setup::new();
    let model = RawModel {
        name: "x".to_string(),
        parser: ParserBody::Or(vec![
            ParserBody::Or(vec![
                ParserBody::Model("m".to_string()).into_prs(),
                ParserBody::Model("std.int".to_string()).into_prs(),
            ])
            .into_prs(),
            ParserBody::Or(vec![
                ParserBody::Range("a".to_string()).into_prs(),
                ParserBody::Token("t".to_string()).into_prs(),
            ])
            .into_prs(),
        ])
        .into_prs(),
        ..Default::default()
    };
    let got = setup.pre_validate(&model).unwrap();
    let exp = ValidatedModel {
        name: "mod.x".to_string(),
        parser: Parser {
            body: ParserBody::Or(vec![
                Parser {
                    body: ParserBody::Or(vec![
                        ParserBody::Model("mod.m".to_string()).into_prs(),
                        ParserBody::Model("std.int".to_string()).into_prs(),
                    ]),
                    ..Default::default()
                },
                Parser {
                    body: ParserBody::Or(vec![
                        ParserBody::Range("mod.a".to_string()).into_prs(),
                        ParserBody::Token("mod.t".to_string()).into_prs(),
                    ]),
                    ..Default::default()
                },
            ]),
            ..Default::default()
        },
        tp: ModelType::Unknown,
        finalizer: Finalizer::Struct,
        fields: vec![],
        pre_validated_fields: vec![],
    };
    assert_eq!(exp, got);
}
