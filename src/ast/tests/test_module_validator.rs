use crate::ast::errors::ValidationError;

use crate::ast::types::model::{
    Finalizer, ModelField, ModelType, Parser, ParserBody, RawModel, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, RawModule, ValidatedModule};
use crate::ast::{module_validator, pre_compiler, IModuleValidator};
use std::collections::BTreeMap;

struct Setup {
    global: PreCompiled,
    mv: Box<dyn IModuleValidator>,
}

impl Setup {
    fn new() -> Self {
        let mut pc = pre_compiler();
        pc.init();
        let global = pc.take();
        Self {
            global,
            mv: Box::new(&**module_validator()),
        }
    }

    fn validate(&self, module: RawModule) -> Result<ValidatedModule, ValidationError> {
        self.mv.validate(&self.global, module)
    }
}

fn raw_module() -> RawModule {
    RawModule {
        name: "mod".to_string(),
        imports: vec!["std".into()],
        alphabets: vec![],
        tokens: vec![("op".to_string(), "[".to_string())],
        models: vec![
            RawModel::token("op".to_string(), false),
            RawModel {
                name: "int".to_string(),
                parser: ParserBody::And(vec![
                    ParserBody::ZeroOrMore(ParserBody::Model("std.space".to_string()).into_bprs())
                        .into_prs(),
                    Parser {
                        field: Some("out".to_string()),
                        body: ParserBody::Model("std.int".to_string()),
                        fatal_err: false,
                    },
                ])
                .into_prs(),
                tp: ModelType::Unknown,
                finalizer: Finalizer::Outfield("out".to_string()),
            },
            RawModel {
                name: "int_list".to_string(),
                parser: Parser {
                    field: Some("out".to_string()),
                    body: ParserBody::OneOrMore(ParserBody::Model("int".to_string()).into_bprs()),
                    fatal_err: false,
                },
                tp: ModelType::Unknown,
                finalizer: Finalizer::Outfield("out".to_string()),
            },
        ],
        custom_code: "".to_string(),
    }
}

fn expected_module() -> ValidatedModule {
    ValidatedModule {
        name: "mod".to_string(),
        imports: vec!["std".into()],
        tokens: BTreeMap::from([("mod.op".to_string(), "[".to_string())]),
        models: BTreeMap::from([
            (
                "mod.op".to_string(),
                ValidatedModel {
                    name: "mod.op".to_string(),
                    parser: Parser {
                        field: Some("<out>".to_string()),
                        body: ParserBody::Token("mod.op".to_string()),
                        fatal_err: false,
                    },
                    tp: ModelType::StdStr,
                    finalizer: Finalizer::Outfield("<out>".to_string()),
                    fields: vec![ModelField {
                        name: "<out>".to_string(),
                        tp: ModelType::StdStr,
                    }],
                    pre_validated_fields: vec![],
                },
            ),
            (
                "mod.int".to_string(),
                ValidatedModel {
                    name: "mod.int".to_string(),
                    parser: ParserBody::And(vec![
                        ParserBody::ZeroOrMore(
                            ParserBody::Model("std.space".to_string()).into_bprs(),
                        )
                        .into_prs(),
                        Parser {
                            field: Some("out".to_string()),
                            body: ParserBody::Model("std.int".to_string()),
                            fatal_err: false,
                        },
                    ])
                    .into_prs(),
                    tp: ModelType::StdInt,
                    finalizer: Finalizer::Outfield("out".to_string()),
                    fields: vec![ModelField {
                        name: "out".to_string(),
                        tp: ModelType::StdInt,
                    }],
                    pre_validated_fields: vec![],
                },
            ),
            (
                "mod.int_list".to_string(),
                ValidatedModel {
                    name: "mod.int_list".to_string(),
                    parser: Parser {
                        field: Some("out".to_string()),
                        body: ParserBody::OneOrMore(
                            ParserBody::Model("mod.int".to_string()).into_bprs(),
                        ),
                        fatal_err: false,
                    },
                    tp: ModelType::List(Box::new(ModelType::StdInt)),
                    finalizer: Finalizer::Outfield("out".to_string()),
                    fields: vec![ModelField {
                        name: "out".to_string(),
                        tp: ModelType::List(Box::new(ModelType::StdInt)),
                    }],
                    pre_validated_fields: vec![],
                },
            ),
        ]),
        ..Default::default()
    }
}

#[test]
fn test_parser_list_of_int() {
    let setup = Setup::new();
    let res = setup.validate(raw_module()).unwrap();
    assert_eq!(res, expected_module())
}

#[test]
fn test_optimize_alph() {
    let mut setup = Setup::new();
    let name = "ext.a".to_string();
    let val = "abc".to_string();
    setup.global.used_names.insert("ext".to_string());
    setup.global.alphabets.insert(name.clone(), val.clone());
    setup.global.denorm_alphabets.insert(val, name);

    let raw_module = RawModule {
        name: "mod".to_string(),
        imports: vec!["ext".into()],
        alphabets: vec![("x".to_string(), "abc".to_string())],
        tokens: vec![],
        models: vec![RawModel {
            name: "m".to_string(),
            parser: Parser {
                body: ParserBody::Range("x".to_string()),
                ..Default::default()
            },
            ..Default::default()
        }],
        custom_code: "".to_string(),
    };

    let module = setup.validate(raw_module).unwrap();
    assert!(module.alphabets.is_empty());
    assert_eq!(
        module.models.get("mod.m").unwrap().parser,
        Parser {
            field: None,
            body: ParserBody::Range("ext.a".to_string()),
            fatal_err: false,
        }
    )
}

#[test]
fn test_optimize_token() {
    let mut setup = Setup::new();
    let name = "ext.t".to_string();
    let val = "abc".to_string();
    setup.global.used_names.insert("ext".to_string());
    setup.global.tokens.insert(name.clone(), val.clone());
    setup.global.denorm_tokens.insert(val, name);

    let raw_module = RawModule {
        name: "mod".to_string(),
        imports: vec!["ext".into()],
        alphabets: vec![],
        tokens: vec![("x".to_string(), "abc".to_string())],
        models: vec![RawModel {
            name: "m".to_string(),
            parser: Parser {
                body: ParserBody::Token("x".to_string()),
                ..Default::default()
            },
            ..Default::default()
        }],
        custom_code: "".to_string(),
    };

    let module = setup.validate(raw_module).unwrap();
    assert!(module.tokens.is_empty());
    assert_eq!(
        module.models.get("mod.m").unwrap().parser,
        Parser {
            field: None,
            body: ParserBody::Token("ext.t".to_string()),
            fatal_err: false,
        }
    )
}
