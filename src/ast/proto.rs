use crate::ast::errors::ValidationError;
use crate::ast::types::model::{
    ModelField, ModelType, Parser, ParserBody, PreValidatedModelField, RawModel, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, RawModule, ValidatedModule};
use mddd::macros::auto_impl;

pub trait IAlphabetMaker {
    fn make(&self, alphabet: &str) -> String;
    fn find_replacement(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        alph_name: &String,
    ) -> Option<String>;
}

pub trait ITokenMaker {
    fn find_replacement(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        token_name: &String,
    ) -> Option<String>;
}

pub trait IParserReplacer {
    fn replace(
        &self,
        fun: &dyn Fn(&ParserBody) -> Option<ParserBody>,
        prs: &Parser,
    ) -> Option<Parser>;
}

pub trait IModelPreValidator {
    fn pre_validate(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        model: &RawModel,
    ) -> Result<ValidatedModel, Box<dyn ToString>>;
}

pub trait IModelTypeDeterminer {
    fn determine_type(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<ModelType, Box<dyn ToString>>;
}

pub trait IModelFieldsDeterminer {
    fn determine_fields(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<Vec<PreValidatedModelField>, Box<dyn ToString>>;
    fn ensure_fields(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<Vec<ModelField>, Box<dyn ToString>>;
}

#[auto_impl(link, dyn)]
pub trait IModuleValidator {
    fn validate(
        &self,
        global: &PreCompiled,
        module: RawModule,
    ) -> Result<ValidatedModule, ValidationError>;
}

pub trait ITypeDeterminer {
    fn deref_type(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        tp: &ModelType,
    ) -> ModelType;
}

pub trait IStdLibFactory {
    fn make(&self) -> ValidatedModule;
}

pub trait IPreCompiler {
    fn init(&mut self);
    fn get(&self) -> &PreCompiled;
    fn add_module(&mut self, module: ValidatedModule);
    fn take(&mut self) -> PreCompiled;
}

impl IPreCompiler for Box<dyn IPreCompiler> {
    fn init(&mut self) {
        (**self).init()
    }
    fn get(&self) -> &PreCompiled {
        (**self).get()
    }
    fn add_module(&mut self, module: ValidatedModule) {
        (**self).add_module(module)
    }
    fn take(&mut self) -> PreCompiled {
        (**self).take()
    }
}
