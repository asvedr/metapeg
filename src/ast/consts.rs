pub struct AstConst {}

impl AstConst {
    pub const STD: &'static str = "std";
    pub const MDL_INT: &'static str = "int";
    pub const MDL_S_INT: &'static str = "sint";
    pub const MDL_REAL: &'static str = "real";
    pub const MDL_S_REAL: &'static str = "sreal";
    pub const MDL_SPACE: &'static str = "space";
    pub const MDL_ANY: &'static str = "any";
}
