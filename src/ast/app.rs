use crate::ast::impls::alphabet_maker::AlphabetMaker;
use crate::ast::impls::model_fields_determiner::ModelFieldsDeterminer;
use crate::ast::impls::model_pre_validator::ModelPreValidator;
use crate::ast::impls::model_type_determiner::ModelTypeDeterminer;
use crate::ast::impls::module_validator::ModuleValidator;
use crate::ast::impls::parser_replacer::ParserReplacer;
use crate::ast::impls::pre_compiler::PreCompiler;
use crate::ast::impls::stdlib_factory::StdlibFactory;
use crate::ast::impls::token_maker::TokenMaker;
use crate::ast::impls::type_determiner::TypeDeterminer;
use crate::ast::proto::{IModuleValidator, IPreCompiler};
use mddd::macros::singleton;

#[inline]
pub fn pre_compiler() -> Box<dyn IPreCompiler> {
    Box::new(PreCompiler::<StdlibFactory>::default())
}

#[singleton]
pub fn module_validator() -> Box<dyn IModuleValidator> {
    type Target = ModuleValidator<
        AlphabetMaker,
        TokenMaker,
        ModelPreValidator,
        ModelTypeDeterminer<TypeDeterminer>,
        ModelFieldsDeterminer<TypeDeterminer>,
        ParserReplacer,
    >;

    Box::new(Target::default())
}
