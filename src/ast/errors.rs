#[derive(Debug)]
pub struct ValidationError {
    pub module: Option<String>,
    pub position: ValidationErrorPosition,
    pub cause: String,
}

#[derive(Debug)]
pub enum ValidationErrorPosition {
    TopLevel,
    Alphabet(String),
    Token(String),
    Model(String),
}

impl ValidationError {
    pub fn toplevel<T: ToString>(cause: T) -> Self {
        Self {
            module: Default::default(),
            position: ValidationErrorPosition::TopLevel,
            cause: cause.to_string(),
        }
    }
    pub fn alphabet<T: ToString>(alph: String, cause: T) -> Self {
        Self {
            module: Default::default(),
            position: ValidationErrorPosition::Alphabet(alph),
            cause: cause.to_string(),
        }
    }
    pub fn token<T: ToString>(tkn: String, cause: T) -> Self {
        Self {
            module: Default::default(),
            position: ValidationErrorPosition::Token(tkn),
            cause: cause.to_string(),
        }
    }
    pub fn model<T: ToString>(mdl: String, cause: T) -> Self {
        Self {
            module: Default::default(),
            position: ValidationErrorPosition::Model(mdl),
            cause: cause.to_string(),
        }
    }
}

impl ToString for ValidationError {
    fn to_string(&self) -> String {
        use ValidationErrorPosition::*;

        let mdl: String = match self.module {
            Some(ref val) => val.to_string(),
            _ => "<none>".to_string(),
        };
        let mut res = format!("ValidationError:\n  module: {}\n", mdl);
        match &self.position {
            TopLevel => (),
            Alphabet(name) => res = format!("{}  alphabet: {}\n", res, name),
            Token(name) => res = format!("{}  token: {}\n", res, name),
            Model(name) => res = format!("{}  model: {}\n", res, name),
        }
        format!("{}  error: {}", res, self.cause)
    }
}
