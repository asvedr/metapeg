use std::fmt;

#[derive(Default)]
pub struct RawModel {
    pub name: String,
    pub parser: Parser,
    pub tp: ModelType,
    pub finalizer: Finalizer,
}

impl RawModel {
    pub fn token(token: String, fatal_err: bool) -> Self {
        let name = token.clone();
        Self {
            name,
            parser: Parser {
                field: Some("<out>".to_string()),
                body: ParserBody::Token(token),
                fatal_err,
            },
            tp: ModelType::Unknown,
            finalizer: Finalizer::Outfield("<out>".to_string()),
        }
    }
}

#[derive(Default, Debug, Eq, PartialEq)]
pub struct ValidatedModel {
    pub name: String,
    pub parser: Parser,
    pub tp: ModelType,
    pub finalizer: Finalizer,
    pub fields: Vec<ModelField>,
    pub pre_validated_fields: Vec<PreValidatedModelField>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ModelField {
    pub name: String,
    pub tp: ModelType,
}

#[derive(Debug, Eq, PartialEq)]
pub struct PreValidatedModelField {
    pub name: String,
    pub variants: Vec<ModelType>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Finalizer {
    Struct,
    Outfield(String),
    Func(String),
}

impl Default for Finalizer {
    fn default() -> Self {
        Self::Struct
    }
}

pub struct ModelItem {
    pub field: String,
    pub parser: Parser,
    pub fatal_on_err: bool,
}

#[derive(Default, Eq, PartialEq, Ord, PartialOrd, Clone, Debug)]
pub struct Parser {
    pub field: Option<String>,
    pub body: ParserBody,
    pub fatal_err: bool,
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone)]
pub enum ParserBody {
    Builtin,
    And(Vec<Parser>),
    IfFollow(Box<Parser>, Box<Parser>),
    IfNotFollow(Box<Parser>, Box<Parser>),
    Or(Vec<Parser>),
    Opt(Box<Parser>),
    ZeroOrMore(Box<Parser>),
    OneOrMore(Box<Parser>),
    Token(String), // token name
    Range(String), // alphabet name
    Model(String),
    Stop,
    // AnyOrder
}

impl Default for ParserBody {
    fn default() -> Self {
        Self::Builtin
    }
}

impl ParserBody {
    #[cfg(test)]
    pub fn into_prs(self) -> Parser {
        Parser {
            body: self,
            ..Default::default()
        }
    }

    #[cfg(test)]
    pub fn into_bprs(self) -> Box<Parser> {
        Box::new(self.into_prs())
    }

    #[cfg(test)]
    pub fn into_named_prs(self, name: &str) -> Parser {
        let mut res = self.into_prs();
        res.field = Some(name.to_string());
        res
    }

    #[cfg(test)]
    pub fn into_named_bprs(self, name: &str) -> Box<Parser> {
        let mut res = Box::new(self.into_prs());
        res.field = Some(name.to_string());
        res
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ModelType {
    Unknown,
    RefToModel(String),
    StdInt,
    StdStr,
    StdReal,
    Model(String),
    List(Box<ModelType>),
    Opt(Box<ModelType>),
    Custom(String),
}

impl Default for ModelType {
    fn default() -> Self {
        Self::Unknown
    }
}

impl ModelType {
    pub fn contains_undetermined(&self) -> bool {
        match self {
            ModelType::Unknown | ModelType::RefToModel(_) => true,
            ModelType::StdInt
            | ModelType::StdStr
            | ModelType::StdReal
            | ModelType::Model(_)
            | ModelType::Custom(_) => false,
            ModelType::List(child) => child.contains_undetermined(),
            ModelType::Opt(child) => child.contains_undetermined(),
        }
    }

    pub fn is_compatible(&self, other: &Self) -> bool {
        use ModelType::*;

        match (self, other) {
            (Unknown, _) | (_, Unknown) | (RefToModel(_), _) | (_, RefToModel(_)) => true,
            (StdInt, StdInt) | (StdStr, StdStr) | (StdReal, StdReal) => true,
            (Model(a), Model(b)) => a == b,
            (Custom(a), Custom(b)) => a == b,
            _ => false,
        }
    }
}

impl fmt::Display for ModelType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ModelType::Unknown => f.write_str("_"),
            ModelType::RefToModel(name) => write!(f, "*{}", name),
            ModelType::StdInt => f.write_str("std.int"),
            ModelType::StdStr => f.write_str("std.str"),
            ModelType::StdReal => f.write_str("std.real"),
            ModelType::Model(name) => write!(f, "${}", name),
            ModelType::List(child) => write!(f, "list[{}]", child),
            ModelType::Opt(child) => write!(f, "opt[{}]", child),
            ModelType::Custom(val) => f.write_str(val),
        }
    }
}

impl ValidatedModel {
    pub fn set_pre_validated(&mut self, fields: Vec<PreValidatedModelField>) {
        self.fields.clear();
        self.fields.reserve(fields.len());
        for pre in fields.iter() {
            let name = pre.name.clone();
            let fld = ModelField {
                name,
                tp: ModelType::Unknown,
            };
            self.fields.push(fld);
        }
        self.pre_validated_fields = fields;
    }

    pub fn fields_are_validated(&self) -> bool {
        if !matches!(self.finalizer, Finalizer::Struct) {
            return true;
        }
        for f in self.fields.iter() {
            if f.tp.contains_undetermined() {
                return false;
            }
        }
        true
    }
}

impl fmt::Display for Parser {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(ref var) = self.field {
            write!(f, "{}:", var)?;
        }
        match &self.body {
            ParserBody::Builtin => f.write_str("<builtin>"),
            ParserBody::And(args) => {
                let args = args
                    .iter()
                    .map(|p| p.to_string())
                    .collect::<Vec<_>>()
                    .join(" + ");
                write!(f, "({})", args)
            }
            ParserBody::IfFollow(a, b) => write!(f, "({} >> {})", a, b),
            ParserBody::IfNotFollow(a, b) => write!(f, "({} >! {})", a, b),
            ParserBody::Or(args) => {
                let args = args
                    .iter()
                    .map(|p| p.to_string())
                    .collect::<Vec<_>>()
                    .join(" | ");
                write!(f, "({})", args)
            }
            ParserBody::Opt(ch) => write!(f, "{}?", ch),
            ParserBody::ZeroOrMore(ch) => write!(f, "{}*", ch),
            ParserBody::OneOrMore(ch) => write!(f, "{}+", ch),
            ParserBody::Token(name) => write!(f, "T{}", name),
            ParserBody::Range(name) => write!(f, "R{}", name),
            ParserBody::Model(name) => write!(f, "${}", name),
            ParserBody::Stop => f.write_str("!"),
        }
    }
}
