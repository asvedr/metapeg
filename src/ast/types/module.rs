use crate::ast::types::model::{RawModel, ValidatedModel};
use std::collections::{BTreeMap, HashMap, HashSet};

pub struct RawModule {
    pub name: String,
    pub imports: Vec<Import>,
    pub alphabets: Vec<(String, String)>,
    pub tokens: Vec<(String, String)>,
    pub models: Vec<RawModel>,
    pub custom_code: String,
}

#[derive(Default, Debug, Eq, PartialEq)]
pub struct ValidatedModule {
    pub name: String,
    pub imports: Vec<Import>,
    pub alphabets: BTreeMap<String, String>,
    pub tokens: BTreeMap<String, String>,
    pub models: BTreeMap<String, ValidatedModel>,
    pub custom_code: String,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Import {
    pub import: String,
    pub as_name: String,
}

impl From<&str> for Import {
    fn from(s: &str) -> Self {
        Self {
            import: s.to_string(),
            as_name: s.to_string(),
        }
    }
}

#[derive(Default)]
pub struct PreCompiled {
    pub alphabets: BTreeMap<String, String>,
    pub tokens: BTreeMap<String, String>,
    pub models: BTreeMap<String, ValidatedModel>,
    pub custom_code: Vec<String>,

    pub used_names: HashSet<String>,
    pub dependencies: HashMap<String, Vec<String>>,
    pub denorm_alphabets: HashMap<String, String>,
    pub denorm_tokens: HashMap<String, String>,
}

impl RawModule {
    pub fn globalize(&self, name: &str) -> Result<String, Box<dyn ToString>> {
        let (pref, name) = match name.split_once('.') {
            None => return Ok(format!("{}.{}", self.name, name)),
            Some(val) => val,
        };
        for imp in self.imports.iter() {
            if imp.as_name == pref {
                return Ok(format!("{}.{}", imp.import, name));
            }
        }
        Err(Box::new(format!("module {} is not imported", pref)))
    }

    pub fn has_alphabet(&self, name: &str) -> bool {
        for (key, _) in self.alphabets.iter() {
            if key == name {
                return true;
            }
        }
        false
    }

    pub fn has_token(&self, name: &str) -> bool {
        for (key, _) in self.tokens.iter() {
            if key == name {
                return true;
            }
        }
        false
    }

    pub fn has_model(&self, name: &str) -> bool {
        for mdl in self.models.iter() {
            if mdl.name == name {
                return true;
            }
        }
        false
    }
}

impl PreCompiled {
    pub fn get_alphabets(&self, mod_name: &str) -> Vec<(&str, &str)> {
        let mut result: Vec<(&str, &str)> = Vec::new();
        for (name, val) in self.alphabets.iter() {
            if name.starts_with(mod_name) {
                result.push((name, val))
            }
        }
        result
    }

    pub fn get_tokens(&self, mod_name: &str) -> Vec<(&str, &str)> {
        let mut result: Vec<(&str, &str)> = Vec::new();
        for (name, val) in self.tokens.iter() {
            if name.starts_with(mod_name) {
                result.push((name, val))
            }
        }
        result
    }

    pub fn get_models(&self, mod_name: &str) -> Vec<(&str, &ValidatedModel)> {
        let mut result: Vec<(&str, &ValidatedModel)> = Vec::new();
        for (name, val) in self.models.iter() {
            if name.starts_with(mod_name) {
                result.push((name, val))
            }
        }
        result
    }
}
