use crate::ast::proto::{IModelFieldsDeterminer, ITypeDeterminer};
use crate::ast::types::model::{
    ModelField, ModelType, Parser, ParserBody, PreValidatedModelField, ValidatedModel,
};
use crate::ast::types::module::{PreCompiled, ValidatedModule};
use std::collections::BTreeMap;

#[derive(Default)]
pub struct ModelFieldsDeterminer<TD: ITypeDeterminer> {
    type_determiner: TD,
}

impl<TD: ITypeDeterminer> ModelFieldsDeterminer<TD> {
    fn determine_parser_fields(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        parser: &Parser,
        no_var_section: &'static str,
        out: &mut BTreeMap<String, Vec<ModelType>>,
    ) -> Result<Option<ModelType>, Box<dyn ToString>> {
        macro_rules! rec_down {
            ($prs:expr) => {
                self.determine_parser_fields(global, module, $prs, no_var_section, out)
            };
            ($prs:expr, $sec:expr) => {
                self.determine_parser_fields(global, module, $prs, $sec, out)
            };
        }

        let parser_tp = match &parser.body {
            ParserBody::Builtin => return Err(Box::new("can not determine builtin")),
            ParserBody::And(args) => {
                for arg in args {
                    rec_down!(arg)?;
                }
                None
            }
            ParserBody::IfFollow(a, b) => {
                rec_down!(a)?;
                rec_down!(b, "if-follow")?;
                None
            }
            ParserBody::IfNotFollow(a, b) => {
                rec_down!(a)?;
                rec_down!(b, "if-not-follow")?;
                None
            }
            ParserBody::Or(args) => {
                for arg in args {
                    rec_down!(arg)?;
                }
                None
            }
            ParserBody::Opt(prs) => {
                let tp = rec_down!(prs)?;
                tp.map(Box::new).map(ModelType::Opt)
            }
            ParserBody::ZeroOrMore(prs) => {
                let tp = rec_down!(prs, "*")?;
                tp.map(Box::new).map(ModelType::List)
            }
            ParserBody::OneOrMore(prs) => {
                let tp = rec_down!(prs, "+")?;
                tp.map(Box::new).map(ModelType::List)
            }
            ParserBody::Token(_) => Some(ModelType::StdStr),
            ParserBody::Range(_) => Some(ModelType::StdStr),
            ParserBody::Model(name) => Some(self.get_model_type(global, module, name)),
            ParserBody::Stop => None,
        };
        let field = match parser.field {
            Some(ref val) => val,
            _ => return Ok(parser_tp),
        };
        if no_var_section != "" {
            let msg = format!("var can not be declared in section '{}'", no_var_section);
            return Err(Box::new(msg));
        }
        let tp = match parser_tp {
            Some(val) => val,
            _ => {
                let msg = format!("can not assign parser with no value to field {}", field,);
                return Err(Box::new(msg));
            }
        };
        if out.contains_key(field) {
            self.modify_existing_field(field, &tp, out)?
        } else {
            self.add_new_field(field, &tp, out)
        }
        Ok(Some(tp))
    }

    fn get_model_type(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &String,
    ) -> ModelType {
        self.type_determiner
            .deref_type(global, module, &ModelType::RefToModel(model.clone()))
    }

    fn add_new_field(
        &self,
        field: &String,
        tp: &ModelType,
        out: &mut BTreeMap<String, Vec<ModelType>>,
    ) {
        out.insert(field.clone(), vec![tp.clone()]);
    }

    fn modify_existing_field(
        &self,
        field: &String,
        tp: &ModelType,
        out: &mut BTreeMap<String, Vec<ModelType>>,
    ) -> Result<(), Box<dyn ToString>> {
        let variants = out.get_mut(field).unwrap();
        for var in variants.iter() {
            if var == tp {
                return Ok(());
            }
            if !var.is_compatible(tp) {
                let msg = format!(
                    "Field {}: types {} and {} are not compatible",
                    field, var, tp
                );
                return Err(Box::new(msg));
            }
        }
        variants.push(tp.clone());
        Ok(())
    }

    fn validate_field(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        raw: &PreValidatedModelField,
    ) -> Result<ModelField, Box<dyn ToString>> {
        let mut last_tp = ModelType::Unknown;
        let mut met_unknown = false;
        for var in raw.variants.iter() {
            if matches!(var, ModelType::Unknown) {
                panic!("ModelType::Unknown is not expected in model field")
            }
            let validated = if var.contains_undetermined() {
                self.type_determiner.deref_type(global, module, &var)
            } else {
                var.clone()
            };
            met_unknown = met_unknown || validated.contains_undetermined();
            if !last_tp.is_compatible(&validated) {
                let msg = format!(
                    "field {}: incompatible types {} and {}",
                    raw.name, last_tp, validated,
                );
                return Err(Box::new(msg));
            }
            last_tp = validated;
        }
        let tp = if met_unknown {
            ModelType::Unknown
        } else {
            last_tp
        };
        Ok(ModelField {
            name: raw.name.clone(),
            tp,
        })
    }
}

impl<TD: ITypeDeterminer> IModelFieldsDeterminer for ModelFieldsDeterminer<TD> {
    fn determine_fields(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<Vec<PreValidatedModelField>, Box<dyn ToString>> {
        let mut fields = BTreeMap::new();
        self.determine_parser_fields(global, module, &model.parser, "", &mut fields)?;
        let as_vec = fields
            .into_iter()
            .map(|(name, variants)| PreValidatedModelField { name, variants })
            .collect();
        Ok(as_vec)
    }

    fn ensure_fields(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<Vec<ModelField>, Box<dyn ToString>> {
        let mut result = vec![];
        for i in 0..model.pre_validated_fields.len() {
            if !model.fields[i].tp.contains_undetermined() {
                result.push(model.fields[i].clone());
                continue;
            }
            let field = self.validate_field(global, module, &model.pre_validated_fields[i])?;
            result.push(field);
        }
        Ok(result)
    }
}
