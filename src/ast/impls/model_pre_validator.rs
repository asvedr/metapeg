use crate::ast::proto::IModelPreValidator;
use crate::ast::types::model::{Parser, ParserBody, RawModel, ValidatedModel};
use crate::ast::types::module::{PreCompiled, RawModule};

#[derive(Default)]
pub struct ModelPreValidator {}

impl ModelPreValidator {
    fn validate_parser(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        parser: &Parser,
    ) -> Result<Parser, Box<dyn ToString>> {
        use ParserBody::*;

        let body = match &parser.body {
            Builtin => return Err(Box::new("can not validate builtin")),
            Model(name) => Model(self.validate_parser_model(global, module, name)?),
            And(args) => And(args
                .iter()
                .map(|arg| self.validate_parser(global, module, arg))
                .collect::<Result<_, _>>()?),
            IfFollow(target, check) => IfFollow(
                Box::new(self.validate_parser(global, module, target)?),
                Box::new(self.validate_parser(global, module, check)?),
            ),
            IfNotFollow(target, check) => IfNotFollow(
                Box::new(self.validate_parser(global, module, target)?),
                Box::new(self.validate_parser(global, module, check)?),
            ),
            Or(args) => Or(args
                .iter()
                .map(|arg| self.validate_parser(global, module, arg))
                .collect::<Result<_, _>>()?),
            Opt(child) => Opt(Box::new(self.validate_parser(global, module, child)?)),
            ZeroOrMore(child) => ZeroOrMore(Box::new(self.validate_parser(global, module, child)?)),
            OneOrMore(child) => OneOrMore(Box::new(self.validate_parser(global, module, child)?)),
            Token(token) => Token(self.validate_token(global, module, token)?),
            Range(alphabet) => Range(self.validate_alphabet(global, module, alphabet)?),
            Stop => Stop,
        };
        Ok(Parser {
            field: parser.field.clone(),
            body,
            fatal_err: parser.fatal_err,
        })
    }

    fn validate_parser_model(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        model: &String,
    ) -> Result<String, Box<dyn ToString>> {
        if module.has_model(model) {
            return module.globalize(model);
        }
        let model = module.globalize(model)?;
        if global.models.contains_key(&model) {
            return Ok(model);
        }
        Err(Box::new(format!("model {} not found", model)))
    }

    fn validate_token(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        token_name: &String,
    ) -> Result<String, Box<dyn ToString>> {
        if module.has_token(token_name) {
            return module.globalize(token_name);
        }
        let token_name = module.globalize(token_name)?;
        if global.tokens.contains_key(&token_name) {
            return Ok(token_name);
        }
        Err(Box::new(format!("token {} not found", token_name)))
    }

    fn validate_alphabet(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        alph_name: &String,
    ) -> Result<String, Box<dyn ToString>> {
        if module.has_alphabet(alph_name) {
            return module.globalize(alph_name);
        }
        let alph_name = module.globalize(alph_name)?;
        if global.alphabets.contains_key(&alph_name) {
            return Ok(alph_name);
        }
        Err(Box::new(format!("alphabet {} not found", alph_name)))
    }
}

impl IModelPreValidator for ModelPreValidator {
    fn pre_validate(
        &self,
        global: &PreCompiled,
        module: &RawModule,
        model: &RawModel,
    ) -> Result<ValidatedModel, Box<dyn ToString>> {
        Ok(ValidatedModel {
            name: module.globalize(&model.name)?,
            parser: self.validate_parser(global, module, &model.parser)?,
            tp: model.tp.clone(),
            finalizer: model.finalizer.clone(),
            fields: vec![],
            pre_validated_fields: vec![],
        })
    }
}
