use crate::ast::proto::IAlphabetMaker;
use crate::ast::types::module::{PreCompiled, ValidatedModule};
use std::collections::BTreeSet;

#[derive(Default)]
pub struct AlphabetMaker {}

impl IAlphabetMaker for AlphabetMaker {
    fn make(&self, alphabet: &str) -> String {
        alphabet
            .chars()
            .collect::<BTreeSet<_>>()
            .into_iter()
            .collect::<String>()
    }

    fn find_replacement(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        alph_name: &String,
    ) -> Option<String> {
        let alph = module.alphabets.get(alph_name)?;
        if let Some(key) = global.denorm_alphabets.get(alph) {
            return Some(key.clone());
        }
        for (key, val) in module.alphabets.iter() {
            if key == alph_name {
                break;
            }
            if val == alph {
                return Some(key.clone());
            }
        }
        None
    }
}
