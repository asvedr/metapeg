use crate::ast::errors::ValidationError;
use crate::ast::proto::{
    IAlphabetMaker, IModelFieldsDeterminer, IModelPreValidator, IModelTypeDeterminer,
    IModuleValidator, IParserReplacer, ITokenMaker,
};
use crate::ast::types::model::{ModelField, ModelType, ParserBody, ValidatedModel};
use crate::ast::types::module::{PreCompiled, RawModule, ValidatedModule};
use std::collections::BTreeMap;

#[derive(Default)]
pub struct ModuleValidator<
    AM: IAlphabetMaker,
    TM: ITokenMaker,
    MPV: IModelPreValidator,
    MTD: IModelTypeDeterminer,
    MFD: IModelFieldsDeterminer,
    PR: IParserReplacer,
> {
    alph_maker: AM,
    token_maker: TM,
    pre_validator: MPV,
    type_determiner: MTD,
    fields_determiner: MFD,
    replacer: PR,
}

impl<
        AM: IAlphabetMaker,
        TM: ITokenMaker,
        MPV: IModelPreValidator,
        MTD: IModelTypeDeterminer,
        MFD: IModelFieldsDeterminer,
        PR: IParserReplacer,
    > ModuleValidator<AM, TM, MPV, MTD, MFD, PR>
{
    fn pre_validate(
        &self,
        global: &PreCompiled,
        raw: RawModule,
    ) -> Result<ValidatedModule, ValidationError> {
        if global.used_names.contains(&raw.name) {
            return Err(ValidationError::toplevel("duplicate module name"));
        }
        for imp in raw.imports.iter() {
            if !global.used_names.contains(&imp.import) {
                let msg = format!("invalid import: {}", imp.import);
                return Err(ValidationError::toplevel(msg));
            }
        }
        let mut alphabets = BTreeMap::new();
        for (name, val) in raw.alphabets.iter() {
            if alphabets.contains_key(name) {
                return Err(ValidationError::alphabet(
                    name.clone(),
                    "name is duplicated",
                ));
            }
            let name = format!("{}.{}", raw.name, name);
            alphabets.insert(name, self.alph_maker.make(val));
        }
        let mut tokens = BTreeMap::new();
        for (name, val) in raw.tokens.iter() {
            if tokens.contains_key(name) {
                return Err(ValidationError::token(name.clone(), "name is duplicated"));
            }
            let name = format!("{}.{}", raw.name, name);
            tokens.insert(name, val.clone());
        }
        let mut models = BTreeMap::new();
        for mdl in raw.models.iter() {
            if models.contains_key(&mdl.name) {
                return Err(ValidationError::model(
                    mdl.name.clone(),
                    "name is duplicated",
                ));
            }
            let v_mdl = self
                .pre_validator
                .pre_validate(global, &raw, mdl)
                .map_err(|err| ValidationError::model(mdl.name.clone(), err.to_string()))?;
            models.insert(v_mdl.name.clone(), v_mdl);
        }
        let res = ValidatedModule {
            name: raw.name,
            imports: raw.imports,
            alphabets,
            tokens,
            models,
            custom_code: raw.custom_code,
        };
        Ok(res)
    }

    fn pre_validate_fields(
        &self,
        global: &PreCompiled,
        validated: &mut ValidatedModule,
    ) -> Result<(), ValidationError> {
        let mut values = Vec::new();
        for (key, mdl) in validated.models.iter() {
            let fields = self
                .fields_determiner
                .determine_fields(global, validated, mdl)
                .map_err(|err| ValidationError::model(key.clone(), err.to_string()))?;
            values.push((key.clone(), fields));
        }
        for (key, fields) in values {
            validated
                .models
                .get_mut(&key)
                .unwrap()
                .set_pre_validated(fields);
        }
        Ok(())
    }

    fn reveal_types_step(
        &self,
        global: &PreCompiled,
        validated: &mut ValidatedModule,
    ) -> Result<(usize, usize), ValidationError> {
        let mut unknown_models = 0;
        let mut unknown_fields = 0;
        let mut to_set = Vec::new();
        for (_, mdl) in validated.models.iter() {
            if self.model_fully_typed(mdl) {
                continue;
            }
            let (tp, fields) = self.reveal_types_model(global, validated, mdl)?;
            if tp.contains_undetermined() {
                unknown_models += 1;
            }
            unknown_fields += fields
                .iter()
                .filter(|f| f.tp.contains_undetermined())
                .count();
            to_set.push((mdl.name.clone(), tp, fields))
        }
        for (name, tp, fields) in to_set {
            let mdl = validated.models.get_mut(&name).unwrap();
            mdl.tp = tp;
            mdl.fields = fields;
        }
        Ok((unknown_models, unknown_fields))
    }

    fn model_fully_typed(&self, mdl: &ValidatedModel) -> bool {
        for fld in mdl.fields.iter() {
            if fld.tp.contains_undetermined() {
                return false;
            }
        }
        !mdl.tp.contains_undetermined()
    }

    fn reveal_types_model(
        &self,
        global: &PreCompiled,
        validated: &ValidatedModule,
        mdl: &ValidatedModel,
    ) -> Result<(ModelType, Vec<ModelField>), ValidationError> {
        let tp = self
            .type_determiner
            .determine_type(global, validated, mdl)
            .map_err(|err| ValidationError::model(mdl.name.clone(), err.to_string()))?;
        let fields = self
            .fields_determiner
            .ensure_fields(global, validated, mdl)
            .map_err(|err| ValidationError::model(mdl.name.clone(), err.to_string()))?;
        Ok((tp, fields))
    }

    fn reveal_types(
        &self,
        global: &PreCompiled,
        validated: &mut ValidatedModule,
    ) -> Result<(), ValidationError> {
        let (mut unknown_models, mut unknown_fields) = self.reveal_types_step(global, validated)?;
        while unknown_fields + unknown_models > 0 {
            let (m, f) = self.reveal_types_step(global, validated)?;
            if m == unknown_models && f == unknown_fields {
                let (mdl, cause) = self.find_unknown(validated);
                return Err(ValidationError::model(mdl, cause));
            }
            unknown_models = m;
            unknown_fields = f;
        }
        Ok(())
    }

    fn clear_pre(&self, validated: &mut ValidatedModule) {
        for mdl in validated.models.values_mut() {
            mdl.pre_validated_fields.clear();
        }
    }

    fn find_unknown(&self, validated: &ValidatedModule) -> (String, String) {
        for (_, mdl) in validated.models.iter() {
            if mdl.tp.contains_undetermined() {
                return (mdl.name.clone(), "type can not be determined".to_string());
            }
            for fld in mdl.fields.iter() {
                if fld.tp.contains_undetermined() {
                    let msg = format!("type of field {} can not be determined", fld.name);
                    return (mdl.name.clone(), msg);
                }
            }
        }
        unreachable!()
    }

    fn optimize_alphabets(&self, global: &PreCompiled, module: &mut ValidatedModule) {
        let mut to_replace = BTreeMap::new();
        for key in module.alphabets.keys() {
            if let Some(rep) = self.alph_maker.find_replacement(global, module, key) {
                to_replace.insert(key.clone(), rep);
            }
        }
        let fun = |body: &ParserBody| -> Option<ParserBody> {
            let alph = match body {
                ParserBody::Range(key) => key,
                _ => return None,
            };
            let rep = to_replace.get(alph)?;
            Some(ParserBody::Range(rep.clone()))
        };
        for mdl in module.models.values_mut() {
            let opt_rep = self.replacer.replace(&fun, &mdl.parser);
            if let Some(rep) = opt_rep {
                mdl.parser = rep
            }
        }
        for key in to_replace.keys() {
            module.alphabets.remove(key);
        }
    }

    fn optimize_tokens(&self, global: &PreCompiled, module: &mut ValidatedModule) {
        let mut to_replace = BTreeMap::new();
        for key in module.tokens.keys() {
            if let Some(rep) = self.token_maker.find_replacement(global, module, key) {
                to_replace.insert(key.clone(), rep);
            }
        }
        let fun = |body: &ParserBody| -> Option<ParserBody> {
            let token = match body {
                ParserBody::Token(key) => key,
                _ => return None,
            };
            let rep = to_replace.get(token)?;
            Some(ParserBody::Token(rep.clone()))
        };
        for mdl in module.models.values_mut() {
            let opt_rep = self.replacer.replace(&fun, &mdl.parser);
            if let Some(rep) = opt_rep {
                mdl.parser = rep
            }
        }
        for key in to_replace.keys() {
            module.tokens.remove(key);
        }
    }

    fn _validate(
        &self,
        global: &PreCompiled,
        raw: RawModule,
    ) -> Result<ValidatedModule, ValidationError> {
        let mut validated = self.pre_validate(global, raw)?;
        self.pre_validate_fields(global, &mut validated)?;
        self.reveal_types(global, &mut validated)?;
        self.clear_pre(&mut validated);
        self.optimize_alphabets(global, &mut validated);
        self.optimize_tokens(global, &mut validated);
        Ok(validated)
    }
}

impl<
        AM: IAlphabetMaker,
        TM: ITokenMaker,
        MPV: IModelPreValidator,
        MTD: IModelTypeDeterminer,
        MFD: IModelFieldsDeterminer,
        PR: IParserReplacer,
    > IModuleValidator for ModuleValidator<AM, TM, MPV, MTD, MFD, PR>
{
    fn validate(
        &self,
        global: &PreCompiled,
        raw: RawModule,
    ) -> Result<ValidatedModule, ValidationError> {
        let name = raw.name.clone();
        self._validate(global, raw).map_err(|mut err| {
            err.module = Some(name);
            err
        })
    }
}
