use crate::ast::proto::{IPreCompiler, IStdLibFactory};
use crate::ast::types::module::{PreCompiled, ValidatedModule};
use std::mem;

#[derive(Default)]
pub struct PreCompiler<F: IStdLibFactory> {
    state: PreCompiled,
    stdlib: F,
}

impl<F: IStdLibFactory> IPreCompiler for PreCompiler<F> {
    fn init(&mut self) {
        let lib = self.stdlib.make();
        self.state = PreCompiled::default();
        self.add_module(lib)
    }

    fn get(&self) -> &PreCompiled {
        &self.state
    }

    fn add_module(&mut self, module: ValidatedModule) {
        let state = &mut self.state;
        let deps = module
            .imports
            .iter()
            .map(|imp| imp.import.clone())
            .collect::<Vec<_>>();
        state.dependencies.insert(module.name.clone(), deps);
        state.used_names.insert(module.name);
        state
            .denorm_alphabets
            .extend(module.alphabets.iter().map(|(k, v)| (v.clone(), k.clone())));
        state
            .denorm_tokens
            .extend(module.tokens.iter().map(|(k, v)| (v.clone(), k.clone())));

        state.alphabets.extend(module.alphabets.into_iter());
        state.tokens.extend(module.tokens.into_iter());
        state.models.extend(module.models.into_iter());
        state.custom_code.push(module.custom_code);
    }

    fn take(&mut self) -> PreCompiled {
        mem::take(&mut self.state)
    }
}
