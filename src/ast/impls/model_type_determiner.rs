use crate::ast::proto::{IModelTypeDeterminer, ITypeDeterminer};
use crate::ast::types::model::{Finalizer, ModelType, ValidatedModel};
use crate::ast::types::module::{PreCompiled, ValidatedModule};

#[derive(Default)]
pub struct ModelTypeDeterminer<TD: ITypeDeterminer> {
    determiner: TD,
}

impl<TD: ITypeDeterminer> ModelTypeDeterminer<TD> {
    fn get_type_by_model_name(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        name: &String,
    ) -> ModelType {
        if let Some(mdl) = module.models.get(name) {
            return mdl.tp.clone();
        }
        if let Some(mdl) = global.models.get(name) {
            return mdl.tp.clone();
        }
        panic!("can not find model: {}", name)
    }

    fn reveal_unknown(&self, model: &ValidatedModel) -> Result<ModelType, Box<dyn ToString>> {
        let fname = match model.finalizer {
            Finalizer::Struct => return Ok(ModelType::Model(model.name.clone())),
            Finalizer::Outfield(ref fld) => fld,
            Finalizer::Func(_) => {
                let msg = "model has custom finalizer, so type must be set explicit";
                return Err(Box::new(msg));
            }
        };
        for field in model.fields.iter() {
            if field.name == *fname {
                return Ok(field.tp.clone());
            }
        }
        let msg = format!("field {} not found", fname);
        Err(Box::new(msg))
    }
}

impl<TD: ITypeDeterminer> IModelTypeDeterminer for ModelTypeDeterminer<TD> {
    fn determine_type(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        model: &ValidatedModel,
    ) -> Result<ModelType, Box<dyn ToString>> {
        if matches!(model.tp, ModelType::Unknown) {
            let tp = self.reveal_unknown(model)?;
            if matches!(tp, ModelType::Unknown) {
                return Ok(ModelType::Unknown);
            }
            if !tp.contains_undetermined() {
                return Ok(tp);
            }
            Ok(self.determiner.deref_type(global, module, &tp))
        } else {
            Ok(self.determiner.deref_type(global, module, &model.tp))
        }
    }
}
