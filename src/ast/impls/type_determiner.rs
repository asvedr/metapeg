use crate::ast::proto::ITypeDeterminer;
use crate::ast::types::model::ModelType;
use crate::ast::types::module::{PreCompiled, ValidatedModule};

#[derive(Default)]
pub struct TypeDeterminer {}

impl ITypeDeterminer for TypeDeterminer {
    fn deref_type(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        tp: &ModelType,
    ) -> ModelType {
        use ModelType::*;

        match tp {
            Unknown => panic!("unknown type must not exists at determine stage"),
            RefToModel(mdl) => {
                let mdl_tp = get_model_type(global, module, mdl);
                if mdl_tp.contains_undetermined() {
                    RefToModel(mdl.clone())
                } else {
                    mdl_tp
                }
            }
            StdInt | StdStr | StdReal | Model(_) | Custom(_) => tp.clone(),
            List(_child) => List(Box::new(self.deref_type(global, module, _child))),
            Opt(_child) => Opt(Box::new(self.deref_type(global, module, _child))),
        }
    }
}

fn get_model_type(global: &PreCompiled, module: &ValidatedModule, name: &String) -> ModelType {
    if let Some(mdl) = module.models.get(name) {
        return mdl.tp.clone();
    }
    if let Some(mdl) = global.models.get(name) {
        return mdl.tp.clone();
    }
    panic!("model {} not found", name)
}
