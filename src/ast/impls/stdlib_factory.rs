use crate::ast::consts::AstConst;
use crate::ast::proto::IStdLibFactory;
use crate::ast::types::model::{Finalizer, ModelType, Parser, ParserBody, ValidatedModel};
use crate::ast::types::module::ValidatedModule;
use std::collections::BTreeMap;

#[derive(Default)]
pub struct StdlibFactory {}

fn model(name: &'static str, tp: ModelType) -> (String, ValidatedModel) {
    let name = format!("{}.{}", AstConst::STD, name);
    let m = ValidatedModel {
        name: name.clone(),
        parser: Parser {
            body: ParserBody::Builtin,
            ..Default::default()
        },
        tp,
        finalizer: Finalizer::Struct,
        fields: vec![],
        pre_validated_fields: vec![],
    };
    (name, m)
}

impl IStdLibFactory for StdlibFactory {
    fn make(&self) -> ValidatedModule {
        ValidatedModule {
            name: AstConst::STD.to_string(),
            models: BTreeMap::from([
                model(AstConst::MDL_SPACE, ModelType::StdStr),
                model(AstConst::MDL_INT, ModelType::StdInt),
                model(AstConst::MDL_S_INT, ModelType::StdStr),
                model(AstConst::MDL_REAL, ModelType::StdReal),
                model(AstConst::MDL_S_REAL, ModelType::StdStr),
                model(AstConst::MDL_ANY, ModelType::StdStr),
            ]),
            ..Default::default()
        }
    }
}
