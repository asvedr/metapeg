use crate::ast::proto::IParserReplacer;
use crate::ast::types::model::{Parser, ParserBody};

#[derive(Default)]
pub struct ParserReplacer {}

impl ParserReplacer {
    fn replace_list(
        &self,
        fun: &dyn Fn(&ParserBody) -> Option<ParserBody>,
        parsers: &[Parser],
    ) -> Option<Vec<Parser>> {
        let mut opts = Vec::new();
        let mut has_some = false;
        for p in parsers {
            let opt_val = self.replace(fun, p);
            has_some = has_some || opt_val.is_some();
            opts.push(opt_val);
        }
        if !has_some {
            return None;
        }
        let res = opts
            .into_iter()
            .enumerate()
            .map(|(i, opt)| match opt {
                Some(val) => val,
                None => parsers[i].clone(),
            })
            .collect::<Vec<_>>();
        Some(res)
    }
}

impl IParserReplacer for ParserReplacer {
    fn replace(
        &self,
        fun: &dyn Fn(&ParserBody) -> Option<ParserBody>,
        prs: &Parser,
    ) -> Option<Parser> {
        use ParserBody::*;

        let new_body = match &prs.body {
            And(args) => And(self.replace_list(fun, args)?),
            IfFollow(a, b) => {
                let opt_a = self.replace(fun, a);
                let opt_b = self.replace(fun, b);
                if opt_a.is_none() && opt_b.is_none() {
                    return None;
                } else {
                    IfFollow(unopt(opt_a, a), unopt(opt_b, b))
                }
            }
            IfNotFollow(a, b) => {
                let opt_a = self.replace(fun, a);
                let opt_b = self.replace(fun, b);
                if opt_a.is_none() && opt_b.is_none() {
                    return None;
                } else {
                    IfNotFollow(unopt(opt_a, a), unopt(opt_b, b))
                }
            }
            Or(args) => Or(self.replace_list(fun, args)?),
            Opt(child) => Opt(Box::new(self.replace(fun, child)?)),
            ZeroOrMore(child) => ZeroOrMore(Box::new(self.replace(fun, child)?)),
            OneOrMore(child) => OneOrMore(Box::new(self.replace(fun, child)?)),
            Token(_) | Range(_) | Model(_) | Stop | Builtin => fun(&prs.body)?,
        };
        Some(Parser {
            field: prs.field.clone(),
            fatal_err: prs.fatal_err,
            body: new_body,
        })
    }
}

fn unopt<T: Clone>(opt: Option<T>, def: &T) -> Box<T> {
    match opt {
        Some(val) => Box::new(val),
        None => Box::new(def.clone()),
    }
}
