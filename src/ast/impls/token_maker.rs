use crate::ast::proto::ITokenMaker;
use crate::ast::types::module::{PreCompiled, ValidatedModule};

#[derive(Default)]
pub struct TokenMaker {}

impl ITokenMaker for TokenMaker {
    fn find_replacement(
        &self,
        global: &PreCompiled,
        module: &ValidatedModule,
        token_name: &String,
    ) -> Option<String> {
        let token = module.tokens.get(token_name)?;
        if let Some(val) = global.denorm_tokens.get(token) {
            return Some(val.clone());
        }
        for (key, val) in module.tokens.iter() {
            if key == token_name {
                break;
            }
            if val == token {
                return Some(key.clone());
            }
        }
        None
    }
}
