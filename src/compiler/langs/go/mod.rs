use crate::compiler::langs::go::external::TCExt;
use crate::compiler::langs::go::internal::TCInt;
use crate::compiler::proto::{IToolChain, IToolChainExternal, IToolChainInternal};

mod consts;
mod external;
mod internal;
mod names;
mod parsers;
mod types;

#[derive(Default)]
pub struct TC {
    int: TCInt,
    ext: TCExt,
}

impl IToolChain for TC {
    fn lang(&self) -> &str {
        "go"
    }

    fn internal(&self) -> &dyn IToolChainInternal {
        &self.int
    }

    fn external(&self) -> &dyn IToolChainExternal {
        &self.ext
    }
}
