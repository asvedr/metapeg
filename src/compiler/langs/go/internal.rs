use crate::ast::types::model::{Finalizer, ModelField, ModelType, ParserBody, ValidatedModel};
use crate::compiler::langs::go::consts::{STD_ERRS, STD_FUNS, STD_TYPES};
use crate::compiler::langs::go::names::Names;
use crate::compiler::langs::go::parsers::ParserTranslator;
use crate::compiler::langs::go::types::Types;
use crate::compiler::proto::IToolChainInternal;
use std::fmt::Write;

#[derive(Default)]
pub struct TCInt {
    names: Names,
    types: Types,
    parsers: ParserTranslator,
}

impl TCInt {
    fn translate_init_vars(&self, fields: &[ModelField]) -> String {
        let mut code = String::new();
        for field in fields {
            let name = self.names.var(&field.name);
            let tp = self.types.translate(&field.tp);
            let _ = writeln!(code, "\tvar {} {}\n", name, tp);
        }
        code
    }

    fn prepare_struct_fields(&self, fields: &[ModelField]) -> String {
        fields
            .iter()
            .map(|f| format!("{}:{}", f.name, self.names.var(&f.name)))
            .collect::<Vec<String>>()
            .join(", ")
    }

    fn translate_finalizer(
        &self,
        model_name: &str,
        fn_tp: &ModelType,
        fields: &[ModelField],
        finalizer: &Finalizer,
    ) -> String {
        let fin = match finalizer {
            Finalizer::Struct => None,
            Finalizer::Outfield(var) => {
                return format!(
                    "\treturn parsed[{}]{}value: {}, cursor: {}{}, nil\n",
                    self.types.translate(fn_tp),
                    '{',
                    self.names.var(var),
                    self.names.cursor(),
                    '}'
                )
            }
            Finalizer::Func(func) => Some(func),
        };
        let sname = self.names.strct(model_name);
        let fields = self.prepare_struct_fields(fields);
        let mut code = format!("strct_res := {}{}{}{}\n", sname, '{', fields, '}');
        if let Some(func) = fin {
            let _ = writeln!(code, "fn_res := {}(strct_res)", func);
            let _ = writeln!(
                code,
                "prs_res := parsed[{}]{}",
                fn_tp, "{value: fn_res, cursor: cursor}"
            );
        } else {
            let _ = writeln!(
                code,
                "prs_res := parsed[{}]{}",
                sname, "{value: strct_res, cursor: cursor}"
            );
            let _ = writeln!(code, "return prs_res, nil");
        }
        code
    }
}

impl IToolChainInternal for TCInt {
    fn stdlib(&self, pkg_name: &str) -> Vec<(String, String)> {
        let prepare = |code: &str| format!("package {}\n{}", pkg_name, code);
        vec![
            ("stderrs.go".to_string(), prepare(STD_ERRS)),
            ("stdfuns.go".to_string(), prepare(STD_FUNS)),
            ("stdtypes.go".to_string(), prepare(STD_TYPES)),
        ]
    }

    fn filename(&self, mod_name: &str) -> String {
        format!("mod_{}.go", mod_name)
    }

    fn header(&self, pkg_name: &str, _mod_name: &str, _imports: &[String]) -> String {
        format!("package {}\n", pkg_name)
    }

    fn begin_declare_part(&self, _mod_name: &str, _imports: &[String]) -> String {
        "".to_string()
    }

    fn end_declare_part(&self, _mod_name: &str) -> String {
        "".to_string()
    }

    fn declare_alphabet(&self, name: &str, val: &str) -> String {
        let mut code = format!("var {} = range_set {}\n", self.names.alph(name), '{');
        let _ = writeln!(code, "\tname: \"{}\"\n", name);
        let _ = writeln!(code, "\tset: map[rune]bool{}\n", '{');
        for sym in val.chars() {
            let _ = writeln!(code, "\t\t{:?}: true", sym);
        }
        let _ = writeln!(code, "\t{}\n{}", '}', '}');
        code
    }

    fn declare_token(&self, name: &str, val: &str) -> String {
        let mut code = format!("var {} = token {}\n", self.names.token(name), '{');
        let _ = writeln!(code, "\tname: \"{}\"", name);
        let _ = writeln!(code, "\trunes: []rune(\"{}\")", val);
        let _ = writeln!(code, "{}", '}');
        code
    }

    fn declare_type(&self, name: &str, val: &ValidatedModel) -> String {
        let mut code = format!("type {} struct {}\n", self.names.strct(name), '{',);
        for field in &val.fields {
            let tp = self.types.translate(&val.tp);
            let _ = writeln!(code, "\t{} {}", field.name, tp);
        }
        let _ = writeln!(code, "{}", '}');
        code
    }

    fn declare_model(&self, name: &str, model: &ValidatedModel) -> String {
        if matches!(model.parser.body, ParserBody::Builtin) {
            return "".to_string();
        }
        let mut code = format!(
            "func {}({} cursor) (parsed[{}], iParseError) {}\n",
            self.names.model(name),
            self.names.cursor(),
            self.types.translate(&model.tp),
            '{',
        );
        code.push_str(&self.translate_init_vars(&model.fields));
        code.push_str(&self.parsers.translate("\t", 0, &model.tp, &model.parser));
        code.push_str(&self.translate_finalizer(
            &model.name,
            &model.tp,
            &model.fields,
            &model.finalizer,
        ));
        code.push_str("}\n");
        code
    }
}
