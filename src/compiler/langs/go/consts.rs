pub const STD_ERRS: &str = r#"
type ParseError struct {
    row    int32
    column int32
    msg    string
}

type iParseError interface {
    to_parse_error() ParseError
    is_fatal() bool
    to_fatal() iParseError
}

type nonFatalErr struct {ParseError}
type fatalErr struct {ParseError}

func (e *nonFatalErr) to_parse_error() ParseError {return e.ParseError}
func (*nonFatalErr) is_fatal() bool {return false}
func (e *nonFatalErr) to_fatal() iParseError {return &fatalErr{ParseError: e.ParseError}}

func (e *fatalErr) to_parse_error() ParseError {return e.ParseError}
func (*fatalErr) is_fatal() bool {return true}
func (e *fatalErr) to_fatal() iParseError {return e}
"#;

pub const STD_TYPES: &str = r#"
type cursor struct {
    runes  []rune
    row    int32
    column int32
}

func new_cursor(src string) cursor {
    return cursor{
        runes:  []rune(src),
        row:    1,
        column: 1,
    }
}

func (c *cursor) shift_mut(count int) []rune {
    token := c.runes[:count]
    for _, sym := range token {
        if sym == '\n' {
            c.row += 1
            c.column = 1
        } else {
            c.column += 1
        }
    }
    c.runes = c.runes[count:]
    return token
}

func (c cursor) shift_no_check(count int) cursor {
    c.runes = c.runes[:count]
    return c
}

func (c *cursor) make_err(msg string) iParseError {
    err := ParseError{row: c.row, column: c.column, msg: msg}
    return &nonFatalErr{ParseError: err}
}

type parsed[T any] struct {
    cursor
    value T
}

type irange interface {
    get_name() string
    is_in(rune) bool
}

type range_set struct {
    set  map[rune]bool
    name string
}

func new_range_set(name string, alph string) irange {
    result := map[rune]bool{}
    for _, sym := range alph {
        result[sym] = true
    }
    return &range_set{set: result, name: name}
}

func (r *range_set) get_name() string {
    return r.name
}

func (r *range_set) is_in(sym rune) bool {
    return r.set[sym]
}

type range_any struct{}

func (r *range_any) get_name() string {
    return "any"
}

func (r range_any) is_in(sym rune) bool {
    return true
}

type token struct {
    name  string
    runes []rune
}

func can_model_from[T any](mdl func(cursor) (parsed[T], iParseError), src cursor) bool {
    _, err := mdl.f(src)
    return err == nil
}
"#;

pub const STD_FUNS: &str = r#"
import "strconv"

func std_get_in_range(rng irange, src cursor) (parsed[string], iParseError) {
	for i, sym := range src.runes {
		if !rng.is_in(sym) {
			value := string(src.shift_mut(i))
			return parsed[string]{value: value, cursor: src}, nil
		}
	}
	value := string(src.shift_mut(len(src.runes)))
	return parsed[string]{value: value, cursor: src}, nil
}

func std_get_in_range_until_model[T any](
	rng irange,
	mdl func(cursor) (parsed[T], iParseError),
	src cursor,
) (parsed[string], iParseError) {
	for i, sym := range src.runes {
		if !rng.is_in(sym) || can_model_from(mdl, src.shift_no_check(i)) {
			value := string(src.shift_mut(i))
			return parsed[string]{value: value, cursor: src}, nil
		}
	}
	value := string(src.shift_mut(len(src.runes)))
	return parsed[string]{value: value, cursor: src}, nil
}

func std_get_token(src cursor, tkn *token) (parsed[string], iParseError) {
	var err iParseError
	if len(src.runes) < len(tkn.runes) {
		err = src.make_err("token " + tkn.name + " not found")
		return parsed[string]{}, err
	}
	for i, sym := range tkn.runes {
		if src.runes[i] != sym {
			err = src.make_err("token " + tkn.name + " not found")
			return parsed[string]{}, err
		}
	}
	src.shift_mut(len(tkn.runes))
	return parsed[string]{
		value:  tkn.name,
		cursor: src,
	}, err
}

var std_space_range = range_set{
	name: "std_space",
	set: map[rune]bool{
		' ': true, '\n': true, '\t': true, '\r': true,
	},
}

var std_digit_range = range_set{
	name: "std_digit",
	set:  map[rune]bool{'0': true, '1': true, '2': true, '3': true, '4': true, '5': true, '6': true, '7': true, '8': true, '9': true},
}

func std_model_space(src cursor) (parsed[string], iParseError) {
	return std_get_in_range(&std_space_range, src)
}

func std_model_s_int(src cursor) (parsed[string], iParseError) {
	return std_get_in_range(&std_digit_range, src)
}

func std_model_int(src cursor) (parsed[int], iParseError) {
	res, p_err := std_model_s_int(src)
	if p_err != nil {
		return parsed[int]{}, p_err
	}
	num, err := strconv.Atoi(res.value)
	if err != nil {
		return parsed[int]{}, src.make_err("can not convert to int")
	}
	return parsed[int]{value: num, cursor: res.cursor}, nil
}

func std_model_s_real(src cursor) (parsed[string], iParseError) {
	res_a, err := std_model_s_int(src)
	if err != nil {
		return parsed[string]{}, err
	}
	if len(res_a.runes) == 0 || res_a.runes[0] != '.' {
		return parsed[string]{}, res_a.make_err("expected '.'")
	}
	src = res_a.cursor
	src.shift_mut(1)
	res_b, err := std_model_s_int(src)
	if err != nil {
		return parsed[string]{}, err
	}
	value := res_a.value + "." + res_b.value
	res_b.value = value
	return res_b, nil
}

func std_model_real(src cursor) (parsed[float64], iParseError) {
	res, p_err := std_model_s_real(src)
	if p_err != nil {
		return parsed[float64]{}, p_err
	}
	value, err := strconv.ParseFloat(res.value, 64)
	if err != nil {
		return parsed[float64]{}, src.make_err("can not convert to float")
	}
	return parsed[float64]{value: value, cursor: res.cursor}, nil
}

func std_or(
	c cursor,
	mdls ...func(cursor) (parsed[std_none], iParseError),
) (parsed[std_none], iParseError) {
	var result parsed[std_none]
	var err iParseError
	for _, mdl := range mdls {
		result, err = mdl(c)
		if err == nil || err.is_fatal() {
			break
		}
	}
	return result, err
}

func std_opt(
	mdl func(cursor) (parsed[std_none], iParseError),
	crs cursor,
) (parsed[std_none], iParseError) {
	result, err := mdl(crs)
	if err == nil {
		return result, nil
	}
	if err.is_fatal() {
		return parsed[std_none]{}, err
	}
	return parsed[std_none]{cursor: crs}, nil
}

func std_zero_or_more[T any](
	mdl func(cursor) (parsed[T], iParseError),
	until func(cursor) (parsed[T], iParseError),
	src cursor,
) (parsed[[]T], iParseError) {
	result := []T{}
	for {
		if until != nil && can_model_from(until, src) {
			break
		}
		m_res, err := mdl(src)
		if err == nil {
			result = append(result, m_res.value)
			src = m_res.cursor
			continue
		}
		if err.is_fatal() {
			return parsed[[]T]{}, err
		} else {
			break
		}
	}
	return parsed[[]T]{value: result, cursor: src}, nil
}

func std_one_or_more[T any](
	mdl func(cursor) (parsed[T], iParseError),
	until func(cursor) (parsed[T], iParseError),
	src cursor,
) (parsed[[]T], iParseError) {
	m_res, err := mdl(src)
	if err != nil {
		return parsed[[]T]{}, err
	}
	// result := []T{m_res.value}
	src = m_res.cursor
	res, err := std_zero_or_more(mdl, until, src)
	if err != nil {
		return res, err
	}
	joined := append([]T{m_res.value}, res.value...)
	res.value = joined
	return res, nil
}
"#;
