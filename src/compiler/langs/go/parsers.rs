use crate::ast::types::model::{ModelType, Parser, ParserBody};
use crate::compiler::langs::go::names::Names;
use crate::compiler::langs::go::types::Types;
use std::fmt::Write;

const STD_INDENT: &str = "\t";

pub struct ParserTranslator {
    names: Names,
    std_none: ModelType,
    types: Types,
}

impl Default for ParserTranslator {
    fn default() -> Self {
        Self {
            names: Default::default(),
            types: Default::default(),
            std_none: ModelType::Custom("std_none".to_string()),
        }
    }
}

impl ParserTranslator {
    fn inc_indent(&self, ind: &str) -> String {
        format!("{}{}", ind, STD_INDENT)
    }

    fn and(&self, indent: &str, index: usize, ret_type: &ModelType, parsers: &[Parser]) -> String {
        let mut code = String::new();
        for (i, prs) in parsers.iter().enumerate() {
            code.push_str(&self.translate(indent, index + i, ret_type, prs));
            // let _ = writeln!(code, "{}if err != nil {}", indent, '{');
            // let _ = writeln!(
            //     code,
            //     "{ind}return parsed[{tp}]{constr}, err",
            //     ind=self.inc_indent(indent),
            //     tp=ret_type,
            //     constr="{}"
            // );
            // let _ = writeln!(code, "{}{}", indent, '}');
            let _ = writeln!(
                code,
                "{}{} = {}.cursor",
                indent,
                self.names.cursor(),
                self.names.res(index + i)
            );
        }
        code
    }

    fn or_case(&self, indent: &str, parser: &Parser) -> String {
        let mut code = format!(
            "{}func({} cursor) (parsed[std_none], iParseError) {}\n",
            indent,
            self.names.cursor(),
            '{'
        );
        code.push_str(&self.translate(&self.inc_indent(indent), 0, &self.std_none, parser));
        let _ = writeln!(code, "{}{},", indent, '}');
        code
    }

    fn or(&self, indent: &str, parsers: &[Parser]) -> String {
        let mut code = format!("{}std_or(\n", indent);
        for prs in parsers {
            code.push_str(&self.or_case(&self.inc_indent(indent), prs));
        }
        let _ = writeln!(code, "{})", indent);
        code
    }

    fn opt(&self, indent: &str, parser: &Parser) -> String {
        let mut code = format!("{}std_opt(\n", indent);
        let indent2 = self.inc_indent(indent);
        code.push_str(&self.or_case(&indent2, parser));
        let _ = writeln!(code, "{}{},", indent2, self.names.cursor());
        let _ = writeln!(code, "{})", indent);
        code
    }

    fn list_item(&self, indent: &str, parser: &Parser) -> String {
        if let ParserBody::Model(mdl) = &parser.body {
            return format!("{}{},\n", indent, self.names.model(mdl));
        }
        let mut code = format!(
            "{}func({}) (parsed[{}], iParseError) {}\n",
            indent,
            self.names.cursor(),
            self.std_none,
            "{"
        );
        let indent2 = self.inc_indent(indent);
        code.push_str(&self.translate(&indent2, 0, &self.std_none, parser));
        let _ = writeln!(
            code,
            "{}return parsed[{}]{}{}{}, nil",
            &indent2,
            self.std_none,
            "{cursor: ",
            self.names.cursor(),
            "}"
        );
        let _ = writeln!(code, "{}{}", indent, '}');
        code
    }

    fn list(
        &self,
        indent: &str,
        list_fun: &str,
        parser: &Parser,
        until: Option<&Parser>,
    ) -> String {
        let mut code = format!("{}{}(\n", indent, list_fun);
        let indent2 = self.inc_indent(indent);
        code.push_str(&self.list_item(&indent2, parser));
        if let Some(prs) = until {
            code.push_str(&self.list_item(&indent2, prs))
        } else {
            let _ = writeln!(code, "{}nil,", indent2);
        }
        let _ = writeln!(code, "{}{},", indent2, self.names.cursor());
        let _ = writeln!(code, "{})", indent);
        code
    }

    fn model(&self, indent: &str, index: usize, name: &str) -> String {
        format!(
            "{}{}, err := {}({})\n",
            indent,
            self.names.res(index),
            self.names.model(name),
            self.names.cursor()
        )
    }

    fn token(&self, indent: &str, index: usize, token: &str) -> String {
        format!(
            "{}{}, err := std_get_token({})\n",
            indent,
            self.names.res(index),
            self.names.token(token),
        )
    }

    fn alph(&self, indent: &str, index: usize, alph: &str) -> String {
        format!(
            "{}{}, err := std_get_sym_in_range({})\n",
            indent,
            self.names.res(index),
            self.names.alph(alph),
        )
    }

    pub fn translate(
        &self,
        indent: &str,
        index: usize,
        ret_type: &ModelType,
        parser: &Parser,
    ) -> String {
        let mut code = match &parser.body {
            ParserBody::Builtin | ParserBody::Stop => panic!(),
            ParserBody::IfFollow(_, _) => panic!("deprecated"),
            ParserBody::IfNotFollow(_, _) => panic!("deprecated"),
            ParserBody::And(args) => self.and(indent, index, ret_type, args),
            ParserBody::Or(args) => self.or(indent, args),
            ParserBody::Opt(arg) => self.opt(indent, arg),
            ParserBody::ZeroOrMore(arg) => self.list(indent, "std_zero_or_more", arg, None),
            ParserBody::OneOrMore(arg) => self.list(indent, "std_one_or_more", arg, None),
            ParserBody::Token(token) => self.token(indent, index, token),
            ParserBody::Range(alph) => self.alph(indent, index, alph),
            ParserBody::Model(mdl) => self.model(indent, index, mdl),
        };
        let _ = writeln!(code, "{}if err != nil {}", indent, '{');
        if parser.fatal_err {
            let _ = writeln!(code, "{}err = err.to_fatal()", self.inc_indent(indent));
        }
        let _ = writeln!(
            code,
            "{ind}return parsed[{tp}]{constr}, err",
            ind = self.inc_indent(indent),
            tp = self.types.translate(ret_type),
            constr = "{}",
        );
        let _ = writeln!(code, "{}{}", indent, '}');
        if let Some(var) = &parser.field {
            let _ = writeln!(
                code,
                "{}{} = {}.value",
                indent,
                self.names.var(var),
                self.names.res(index)
            );
        }
        code
    }
}
