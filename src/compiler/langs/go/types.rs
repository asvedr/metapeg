use crate::ast::types::model::ModelType;
use crate::compiler::langs::go::names::Names;

#[derive(Default)]
pub struct Types {
    names: Names,
}

impl Types {
    pub fn translate(&self, tp: &ModelType) -> String {
        match tp {
            ModelType::Unknown => unreachable!(),
            ModelType::RefToModel(_) => unreachable!(),
            ModelType::StdInt => "int".to_string(),
            ModelType::StdStr => "string".to_string(),
            ModelType::StdReal => "float64".to_string(),
            ModelType::Model(name) => self.names.strct(name),
            ModelType::List(child) => format!("[]{}", self.translate(child)),
            ModelType::Opt(child) => format!("[]{}", self.translate(child)),
            ModelType::Custom(code) => code.clone(),
        }
    }
}
