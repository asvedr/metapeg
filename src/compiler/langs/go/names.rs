use crate::ast::AstConst;

#[derive(Default)]
pub struct Names {}

impl Names {
    pub fn cursor(&self) -> &str {
        "crs"
    }

    pub fn module(&self, name: &str) -> String {
        format!("module_{}", name)
    }

    pub fn alph(&self, name: &str) -> String {
        let (a, b) = name.split_once('.').unwrap();
        format!("alph_{}_{}", a, b)
    }

    pub fn token(&self, name: &str) -> String {
        let (a, b) = name.split_once('.').unwrap();
        format!("tkn_{}_{}", a, b)
    }

    pub fn strct(&self, name: &str) -> String {
        let (a, b) = name.split_once('.').unwrap();
        format!("Type_{}_{}", a, b)
    }

    pub fn model(&self, name: &str) -> String {
        let (a, b) = name.split_once('.').unwrap();
        if a == "std" {
            return self.std_model(b);
        }
        format!("mdl_{}_{}", a, b)
    }

    pub fn var(&self, name: &str) -> String {
        format!("v_{}", name)
    }

    pub fn res(&self, id: usize) -> String {
        format!("res_{}", id)
    }

    fn std_model(&self, name: &str) -> String {
        match () {
            _ if name == AstConst::MDL_INT => "std_model_int",
            _ if name == AstConst::MDL_S_INT => "std_model_s_int",
            _ if name == AstConst::MDL_REAL => "std_model_real",
            _ if name == AstConst::MDL_S_REAL => "std_model_s_real",
            _ if name == AstConst::MDL_SPACE => "std_model_space",
            // _ if name == AstConst::MDL_ANY => "string",
            _ => panic!(""),
        }
        .to_string()
    }
}
