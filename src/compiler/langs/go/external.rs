use crate::ast::types::module::PreCompiled;
use crate::compiler::langs::go::names::Names;
use crate::compiler::langs::go::types::Types;
use crate::compiler::proto::IToolChainExternal;
use std::fmt::Write;

#[derive(Default)]
pub struct TCExt {
    names: Names,
    types: Types,
}

impl IToolChainExternal for TCExt {
    fn filename(&self) -> &str {
        "external.go"
    }

    fn header(&self, pkg_name: &str, _models: &[&str]) -> String {
        let code = format!("package {}\n\n", pkg_name);
        code
    }

    fn model(&self, state: &PreCompiled, name: &str) -> String {
        let mdl = match state.models.get(&name.to_string()) {
            Some(mdl) => mdl,
            _ => panic!("model {} not found", name),
        };
        let name = match name.split_once('.') {
            None => panic!("invalid name: {}", name),
            Some((p, n)) => format!("Parser_{}_{}", p, n),
        };
        let tp = self.types.translate(&mdl.tp);
        let mut code = format!("func {}(src string) ({}, *ParseError) {}\n", name, tp, '{');
        let _ = writeln!(
            code,
            "\tparsed, p_err := {}(std_make_cursor(src))",
            self.names.model(&mdl.name),
        );
        code.push_str(concat!(
            "\tif p_err != nil {\n",
            "\t\treturn parsed.value, p_err.to_parse_error()\n",
            "\t}\n",
            "\treturn parsed.value, nil\n",
            "}\n"
        ));
        code
    }
}
