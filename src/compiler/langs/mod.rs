use crate::compiler::proto::IToolChain;
use mddd::macros::singleton;

mod go;

#[singleton]
pub fn tc_go() -> Box<dyn IToolChain> {
    Box::new(go::TC::default())
}
