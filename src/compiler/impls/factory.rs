use crate::ast::types::module::PreCompiled;
use crate::compiler::impls::translator::Translator;
use crate::compiler::proto::{IToolChain, ITranslator, ITranslatorFactory};

#[derive(Default)]
pub struct TranslatorFactory {}

impl ITranslatorFactory for TranslatorFactory {
    fn make(&self, state: PreCompiled, toolchain: &'static dyn IToolChain) -> Box<dyn ITranslator> {
        Box::new(Translator { state, toolchain })
    }
}
