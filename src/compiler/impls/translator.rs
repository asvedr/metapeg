use crate::ast::types::model::Finalizer;
use crate::ast::types::module::PreCompiled;
use crate::compiler::proto::{IToolChain, ITranslator};
use std::fmt::Write;

pub struct Translator<TC: IToolChain> {
    pub state: PreCompiled,
    pub toolchain: TC,
}

impl<TC: IToolChain> Translator<TC> {
    fn gen_int_mod(&self, pkg_name: &str, mod_name: &str) -> (String, String) {
        let tc_int = self.toolchain.internal();
        let imports = self.state.dependencies.get(mod_name).unwrap();
        let mut code = tc_int.header(pkg_name, mod_name, imports);
        let alphabets = self.state.get_alphabets(mod_name);
        let tokens = self.state.get_tokens(mod_name);
        let models = self.state.get_models(mod_name);
        for (name, val) in &models {
            if !matches!(val.finalizer, Finalizer::Struct | Finalizer::Func(_)) {
                continue;
            }
            let _ = writeln!(code, "{}", tc_int.declare_type(name, val));
        }
        let _ = writeln!(code, "{}", tc_int.begin_declare_part(mod_name, imports));
        for (name, val) in &alphabets {
            let _ = writeln!(code, "{}", tc_int.declare_alphabet(name, val));
        }
        for (name, val) in &tokens {
            let _ = writeln!(code, "{}", tc_int.declare_token(name, val));
        }
        for (name, val) in &models {
            let _ = writeln!(code, "{}", tc_int.declare_model(name, val));
        }
        let _ = writeln!(code, "{}", tc_int.end_declare_part(mod_name));
        // let _ = writeln!(code, "{}", tc_int.begin_init_part(mod_name));
        // for (name, val) in alphabets {
        //     let _ = writeln!(code, "{}", tc_int.init_alphabet(name, val));
        // }
        // for (name, val) in tokens {
        //     let _ = writeln!(code, "{}", tc_int.init_token(name, val));
        // }
        // for (name, val) in models {
        //     let _ = writeln!(code, "{}", tc_int.init_model(name, val));
        // }
        // let _ = writeln!(code, "{}", tc_int.end_init_part(mod_name));
        (tc_int.filename(mod_name), code)
    }

    fn gen_ext_mod(&self, pkg_name: &str, models: &[&str]) -> (String, String) {
        let tc_ext = self.toolchain.external();
        let mut header = tc_ext.header(pkg_name, models);
        header.push('\n');
        for model in models {
            header.push_str(&tc_ext.model(&self.state, model));
            header.push('\n');
        }
        (tc_ext.filename().to_string(), header)
    }
}

impl<TC: IToolChain> ITranslator for Translator<TC> {
    fn gen_files(&self, pkg_name: &str, models: &[&str]) -> Vec<(String, String)> {
        let tc_int = self.toolchain.internal();
        let mut result = tc_int.stdlib(pkg_name);
        for mod_name in &self.state.used_names {
            result.push(self.gen_int_mod(pkg_name, &mod_name));
        }
        result.push(self.gen_ext_mod(pkg_name, models));
        result
    }
}
