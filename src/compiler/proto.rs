use crate::ast::types::model::ValidatedModel;
use crate::ast::types::module::PreCompiled;
use mddd::macros::auto_impl;

pub trait ITranslator {
    fn gen_files(&self, pkg_name: &str, models: &[&str]) -> Vec<(String, String)>;
}

pub trait ITranslatorFactory {
    fn make(&self, state: PreCompiled, toolchain: &'static dyn IToolChain) -> Box<dyn ITranslator>;
}

pub trait IToolChainInternal {
    fn stdlib(&self, pkg_name: &str) -> Vec<(String, String)>;
    fn filename(&self, mod_name: &str) -> String;
    fn header(&self, pkg_name: &str, mod_name: &str, imports: &[String]) -> String;
    fn begin_declare_part(&self, mod_name: &str, imports: &[String]) -> String;
    fn declare_alphabet(&self, name: &str, val: &str) -> String;
    fn declare_token(&self, name: &str, val: &str) -> String;
    fn declare_type(&self, name: &str, val: &ValidatedModel) -> String;
    fn declare_model(&self, name: &str, model: &ValidatedModel) -> String;
    fn end_declare_part(&self, mod_name: &str) -> String;
    // fn begin_init_part(&self, mod_name: &str) -> String;
    // fn init_alphabet(&self, name: &str, val: &str) -> String;
    // fn init_token(&self, name: &str, val: &str) -> String;
    // fn init_model(&self, name: &str, model: &ValidatedModel) -> String;
    // fn end_init_part(&self, mod_name: &str) -> String;
}

pub trait IToolChainExternal {
    fn filename(&self) -> &str;
    fn header(&self, pkg_name: &str, models: &[&str]) -> String;
    fn model(&self, state: &PreCompiled, name: &str) -> String;
}

#[auto_impl(link, dyn)]
pub trait IToolChain {
    fn lang(&self) -> &str;
    fn internal(&self) -> &dyn IToolChainInternal;
    fn external(&self) -> &dyn IToolChainExternal;
}
