use mddd::macros::singleton;

use crate::compiler::impls::factory::TranslatorFactory;
use crate::compiler::langs::tc_go;
use crate::compiler::proto::IToolChain;

pub fn translator_factory() -> TranslatorFactory {
    TranslatorFactory::default()
}

#[singleton]
pub fn toolchains() -> Vec<&'static dyn IToolChain> {
    vec![&**tc_go()]
}
