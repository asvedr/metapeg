use crate::ast::types::model::{
    Finalizer, ModelField, ModelType, ParserBody, ValidatedModel,
};
use crate::ast::types::module::{ValidatedModule};
use crate::ast::{pre_compiler, IPreCompiler};
use crate::compiler::app::translator_factory;
use crate::compiler::langs::tc_go;
use crate::compiler::proto::ITranslatorFactory;
use std::collections::{BTreeMap, HashMap};

fn gen_modules(module: ValidatedModule, pkg: &str, models: &[&str]) -> (String, String) {
    let mut pc = pre_compiler();
    pc.add_module(module);
    let state = pc.take();
    let tr = translator_factory().make(state, &**tc_go());
    let files = tr.gen_files(pkg, models);
    let as_map = files.into_iter().collect::<HashMap<_, _>>();
    let internal = as_map.get("mod_mod.go").unwrap().clone();
    let external = as_map.get("external.go").unwrap().clone();
    (internal, external)
    // let mod_name = "external.go";
    // for (name, data) in files {
    //     if name == mod_name {
    //         return data
    //     }
    // }
    // panic!("module '{}' not found in {:?}", mod_name, names)
}

const EXPECTED_INT_WITH_IDENT_INTERNAL: &str = r#"
package myprs

func mdl_mod_int(crs cursor) (parsed[int], iParseError) {
	var v_val int

	res_0, err := std_model_space(crs)
	if err != nil {
		return parsed[int]{}, err
	}
	crs = res_0.cursor
	res_1, err := std_model_int(crs)
	if err != nil {
		return parsed[int]{}, err
	}
	v_val = res_1.value
	crs = res_1.cursor
	if err != nil {
		return parsed[int]{}, err
	}
	return parsed[int]{value: v_val, cursor: crs}, nil
}
"#;
const EXPECTED_INT_WITH_IDENT_EXTERNAL: &str = r#"
package myprs


func Parser_mod_int(src string) (int, *ParseError) {
	parsed, p_err := mdl_mod_int(std_make_cursor(src))
	if p_err != nil {
		return parsed.value, p_err.to_parse_error()
	}
	return parsed.value, nil
}
"#;

#[test]
fn test_int_with_ident() {
    let model = ValidatedModel {
        name: "mod.int".to_string(),
        parser: ParserBody::And(vec![
            ParserBody::Model("std.space".to_string()).into_prs(),
            ParserBody::Model("std.int".to_string()).into_named_prs("val"),
        ])
        .into_prs(),
        tp: ModelType::StdInt,
        finalizer: Finalizer::Outfield("val".to_string()),
        fields: vec![ModelField {
            name: "val".to_string(),
            tp: ModelType::StdInt,
        }],
        ..Default::default()
    };
    let (int, ext) = gen_modules(
        ValidatedModule {
            name: "mod".to_string(),
            imports: vec!["std".into()],
            models: BTreeMap::from([("mod.int".to_string(), model)]),
            ..Default::default()
        },
        "myprs",
        &["mod.int"],
    );
    println!(">>\n{}\n", ext);
    assert_eq!(int.trim(), EXPECTED_INT_WITH_IDENT_INTERNAL.trim(),);
    assert_eq!(ext.trim(), EXPECTED_INT_WITH_IDENT_EXTERNAL.trim(),);
}
